<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'status', 'style_no', 'price', 'orig_price', 'pack_id', 'sorting', 'description', 'guest_image', 'available_on',
        'availability', 'name', 'default_parent_category', 'default_second_category', 'default_third_category', 'exclusive',
        'min_qty', 'made_in_id', 'labeled', 'memo', 'activated_at', 'fabric', 'video', 'youtube_url', 'min_order'
    ];

    public function colors() {
        return $this->belongsToMany('App\Model\Color')->withPivot('available');
    }

    public function category() {
        return $this->belongsTo('App\Model\Category');
    }

    public function images() {
        return $this->hasMany('App\Model\ItemImages')->orderBy('sort');
    }

    public function vendor() {
        return $this->belongsTo('App\Model\MetaVendor', 'vendor_meta_id', 'id');
    }

    public function pack() {
        return $this->belongsTo('App\Model\Pack');
    }

    public function madeInCountry() {
        return $this->belongsTo('App\Model\MadeInCountry', 'made_in_id');
    }

    public function bodySize() {
        return $this->belongsTo('App\Model\BodySize', 'body_size_id');
    }

    public function pattern() {
        return $this->belongsTo('App\Model\Pattern', 'pattern_id');
    }

    public function length() {
        return $this->belongsTo('App\Model\Length', 'length_id');
    }

    public function style() {
        return $this->belongsTo('App\Model\Style', 'style_id');
    }

    public function categories() {
        return $this->belongsToMany('App\Model\Category');
    }

    protected static function boot() {
        parent::boot();
        static::deleting(function(Item $item) {
            foreach ($item->images as $image)
                $image->delete();
        });
    }
}
