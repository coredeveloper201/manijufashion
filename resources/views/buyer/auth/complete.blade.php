@extends('layouts.home_layout')

@section('content')
    <div class="shipping_cart_area">
    	<div class="container">
			<div class="content" align='center'>
	        	<div class="complete-title mb-1"><h3>Registration Complete</h3><hr></div>
	            <p>Thank you for registering for a customer account. Your profile has been submitted and your account is now under review! It may take up to 1-2 business days for your account to be approved.</p>

	            <p>Thank you for your patience.</p>
	            <p><b>Best Regards,</b></p>
	            <p>{{$_ENV['SITE_NAME']}}</p>
	        </div>
    	</div>
    </div>

@stop