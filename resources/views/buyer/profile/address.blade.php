@extends('layouts.my_account')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
<form action="{{ route('buyer_update_address') }}" method="post">
    @csrf

    <h4>Shipping Address</h4>
    <hr class="padding-bottom-1x">

    <div class="row mb-4">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddShippingAddress">Add New Shipping Address</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 table-responsive padding-top-1x">
            <table class="table table-bordered">
                <tr>
                    <th>Address</th>
                    <th class="text-center">Default</th>
                    <th class="text-center">Action</th>
                </tr>
                @foreach($shippingAddress as $address)
                    <tr>
                        <td>
                            <b>{{ $address->store_no }}</b>
                            {{ $address->address }}, {{ $address->city }}, {{ ($address->state == null) ? $address->state_text : $address->state->name }},
                            <br>
                            {{ $address->country->name }} - {{ $address->zip }}
                        </td>

                        <td class="text-center">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input defaultAddress" type="radio" id="default_address_{{ $address->id }}"
                                       name="defaultAddress" {{ ($address->default == 1) ? 'checked' : '' }} value="{{ $address->id }}">
                                <label class="custom-control-label" for="default_address_{{ $address->id }}"></label>
                            </div>
                        </td>

                        <td class="text-center">
                            <a class="text-info btnEdit" role="button" data-id="{{ $address->id }}" data-index="{{ $loop->index }}">Edit</a> |
                            <a class="text-danger btnDelete" role="button" data-id="{{ $address->id }}">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    <h4>Billing Address</h4>
    <hr class="padding-bottom-1x">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="small-rounded-input">Location</label><br>
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationUS" name="factoryLocation" value="US"
                            {{ empty(old('factoryLocation')) ? ($buyer->billing_location == "US" ? 'checked' : '') :
                                    (old('factoryLocation') == 'US' ? 'checked' : '') }}>
                    <label class="custom-control-label" for="factoryLocationUS">United States</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationCA" name="factoryLocation" value="CA"
                            {{ empty(old('factoryLocation')) ? ($buyer->billing_location == "CA" ? 'checked' : '') :
                                    (old('factoryLocation') == 'CA' ? 'checked' : '') }}>
                    <label class="custom-control-label" for="factoryLocationCA">Canada</label>
                </div>

                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input factoryLocation" type="radio" id="factoryLocationInt" name="factoryLocation" value="INT"
                            {{ empty(old('factoryLocation')) ? ($buyer->billing_location == "INT" ? 'checked' : '') :
                                    (old('factoryLocation') == 'INT' ? 'checked' : '') }}>
                    <label class="custom-control-label" for="factoryLocationInt">International</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5">
            <div class="form-group{{ $errors->has('factoryAddress') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Address <span class="required">*</span></label>
                <input class="form-control" type="text" id="factoryAddress" name="factoryAddress"
                       value="{{ empty(old('factoryAddress')) ? ($errors->has('factoryAddress') ? '' : $buyer->billing_address) : old('factoryAddress') }}">

                @if ($errors->has('factoryAddress'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('factoryAddress') }}</div>
                @endif
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group{{ $errors->has('factoryUnit') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Unit #</label>
                <input class="form-control" type="text" id="factoryUnit" name="factoryUnit"
                       value="{{ empty(old('factoryUnit')) ? ($errors->has('factoryUnit') ? '' : $buyer->billing_unit) : old('factoryUnit') }}">
            </div>
        </div>

        <div class="col-md-5">
            <div class="form-group{{ $errors->has('factoryCity') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">City <span class="required">*</span></label>
                <input class="form-control" type="text" id="factoryCity" name="factoryCity"
                       value="{{ empty(old('factoryCity')) ? ($errors->has('factoryCity') ? '' : $buyer->billing_city) : old('factoryCity') }}">

                @if ($errors->has('factoryCity'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('factoryCity') }}</div>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryState') ? ' has-danger' : '' }}" id="form-group-factory-state">
                <label for="small-rounded-input">State <span class="required">*</span></label>
                <input class="form-control" type="text" id="factoryState" name="factoryState"
                       value="{{ empty(old('factoryState')) ? ($errors->has('factoryState') ? '' : $buyer->billing_state) : old('factoryState') }}">

                @if ($errors->has('factoryState'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('factoryState') }}</div>
                @endif
            </div>

            <div class="form-group{{ $errors->has('factoryStateSelect') ? ' has-danger' : '' }}" id="form-group-factory-state-select">
                <label for="small-rounded-input">State <span class="required">*</span></label>
                <select class="form-control" id="factoryStateSelect" name="factoryStateSelect">
                    <option value="">Select State</option>
                </select>

                @if ($errors->has('factoryStateSelect'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('factoryStateSelect') }}</div>
                @endif
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryZipCode') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                <input class="form-control" type="text" id="factoryZipCode" name="factoryZipCode"
                       value="{{ empty(old('factoryZipCode')) ? ($errors->has('factoryZipCode') ? '' : $buyer->billing_zip) : old('factoryZipCode') }}">

                @if ($errors->has('factoryZipCode'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('factoryZipCode') }}</div>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            <div class="form-group{{ $errors->has('factoryCountry') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Country <span class="required">*</span></label>
                <select class="form-control" id="factoryCountry" name="factoryCountry">
                    <option value="">Select Country</option>
                    @foreach($countries as $country)
                        <option data-code="{{ $country->code }}" value="{{ $country->id }}"
                                {{ empty(old('factoryCountry')) ? ($errors->has('factoryCountry') ? '' : ($buyer->billing_country_id == $country->id ? 'selected' : '')) :
                                    (old('factoryCountry') == $country->id ? 'selected' : '') }}>{{ $country->name }}</option>
                    @endforeach
                </select>

                @if ($errors->has('factoryCountry'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('factoryCountry') }}</div>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryPhone') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Phone <span class="required">*</span></label>
                <input class="form-control" type="text" id="factoryPhone" name="factoryPhone"
                       value="{{ empty(old('factoryPhone')) ? ($errors->has('factoryPhone') ? '' : $buyer->billing_phone) : old('factoryPhone') }}">

                @if ($errors->has('factoryPhone'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('factoryPhone') }}</div>
                @endif
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group{{ $errors->has('factoryFax') ? ' has-danger' : '' }}">
                <label for="small-rounded-input">Fax</label>
                <input class="form-control" type="text" id="factoryFax" name="factoryFax"
                       value="{{ empty(old('factoryFax')) ? ($errors->has('factoryFax') ? '' : $buyer->billing_fax) : old('factoryFax') }}">

                @if ($errors->has('factoryFax'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('factoryFax') }}</div>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="factoryCommercial" name="factoryCommercial" value="1"
                            {{ empty(old('factoryCommercial')) ? ($buyer->billing_commercial == 1 ? 'checked' : '') :
                                    (old('factoryCommercial') ? 'checked' : '') }}>
                    <label class="custom-control-label" for="factoryCommercial">This address is commercial.</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <input class="btn btn-primary margin-bottom-none" type="submit" value="UPDATE">
        </div>
    </div>
</form>

<div class="modal fade" id="addEditShippingModal" tabindex="9999" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="modalForm">
            <input type="hidden" id="editAddressId" name="id">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Shipping Address</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="small-rounded-input">Location</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input location" type="radio" id="locationUS" name="location" value="US" checked>
                                    <label class="custom-control-label" for="locationUS">United States</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input location" type="radio" id="locationCA" name="location" value="CA">
                                    <label class="custom-control-label" for="locationCA">Canada</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input location" type="radio" id="locationInt" name="location" value="INT">
                                    <label class="custom-control-label" for="locationInt">International</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Store No.</label>
                                <input class="form-control form-control-rounded form-control-sm" type="text" id="store_no" name="store_no">
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="form-group" id="form-group-address">
                                <label for="small-rounded-input">Address <span class="required text-danger">*</span></label>
                                <input required class="form-control form-control-rounded form-control-sm" type="text" id="address" name="address">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="small-rounded-input">Unit #  </label>
                                <input required class="form-control form-control-rounded form-control-sm" type="text" id="unit" name="unit">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="form-group-city">
                                <label for="small-rounded-input">City <span class="required text-danger">*</span></label>
                                <input class="form-control form-control-rounded form-control-sm" type="text" id="city" name="city">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group" id="form-group-state">
                                <label for="small-rounded-input">State <span class="required text-danger">*</span></label>
                                <input class="form-control form-control-rounded form-control-sm" type="text" id="state" name="state">
                            </div>

                            <div class="form-group" id="form-group-state-select">
                                <label for="small-rounded-input">State <span class="required text-danger">*</span></label>
                                <select class="form-control form-control-rounded form-control-sm" id="stateSelect" name="stateSelect">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="form-group-country">
                                <label for="small-rounded-input">Country <span class="required text-danger">*</span></label>
                                <select class="form-control form-control-rounded form-control-sm" id="country" name="country">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                        <option data-code="{{ $country->code }}" value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group" id="form-group-zip">
                                <label for="small-rounded-input">Zip Code <span class="required text-danger">*</span></label>
                                <input class="form-control form-control-rounded form-control-sm" type="text" id="zipCode" name="zipCode">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="form-group-phone">
                                <label for="small-rounded-input">Phone <span class="required text-danger">*</span></label>
                                <input class="form-control form-control-rounded form-control-sm" type="text" id="phone" name="phone">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="small-rounded-input">Fax</label>
                                <input class="form-control form-control-rounded form-control-sm" type="text" id="fax" name="fax">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="showroomCommercial" name="showroomCommercial" value="1">
                                    <label class="custom-control-label" for="showroomCommercial">This address is commercial.</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-sm" type="button" id="modalBtnAdd">Add</button>
                    <button class="btn btn-primary btn-sm" type="button" id="modalBtnUpdate">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure want to delete?
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary btn-sm" type="button" id="modalBtnDelete">Delete</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        var usStates = <?php echo json_encode($usStates); ?>;
        var caStates = <?php echo json_encode($caStates); ?>;
        var oldFactoryState = '{{ empty(old('factoryStateSelect')) ? ($errors->has('factoryStateSelect') ? '' : $buyer->billing_state_id) : old('factoryStateSelect') }}';
        var shippingAddresses = <?php echo json_encode($shippingAddress); ?>;

        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            $('form').bind('submit', function () {
                $(this).find(':input').prop('disabled', false);
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);


            $('.factoryLocation').change(function () {
                var location = $('.factoryLocation:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#factoryCountry').val('1');
                    else
                        $('#factoryCountry').val('2');

                    $('#factoryCountry').prop('disabled', 'disabled');
                    $('#form-group-factory-state-select').show();
                    $('#factoryStateSelect').val('');
                    $('#form-group-factory-state').hide();

                    $('#factoryStateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            if (value.id == oldFactoryState)
                                $('#factoryStateSelect').append('<option value="'+value.id+'" selected>'+value.name+'</option>');
                            else
                                $('#factoryStateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#factoryCountry').prop('disabled', false);
                    $('#form-group-factory-state-select').hide();
                    $('#form-group-factory-state').show();
                }
            });

            $('.factoryLocation').trigger('change');

            // Shipping Address
            $('#btnAddShippingAddress').click(function (e) {
                e.preventDefault();
                $('#addEditShippingModal').modal('show');
                $('#modalBtnAdd').show();
                $('#modalBtnUpdate').hide();
            });

            $('.location').change(function () {
                var location = $('.location:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#country').val('1');
                    else
                        $('#country').val('2');

                    $('#country').prop('disabled', 'disabled');
                    $('#form-group-state-select').show();
                    $('#stateSelect').val('');
                    $('#form-group-state').hide();

                    $('#stateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#country').prop('disabled', false);
                    $('#form-group-state-select').hide();
                    $('#form-group-state').show();
                    $('#country').val('');
                }
            });

            $('.location').trigger('change');

            $('#country').change(function () {
                var countryId = $(this).val();

                if (countryId == 1) {
                    $("#locationUS").prop("checked", true);
                    $('.location').trigger('change');
                } else if (countryId == 2) {
                    $("#locationCA").prop("checked", true);
                    $('.location').trigger('change');
                }
            });
            
            $('#modalBtnAdd').click(function () {
                if (!shippingAddressValidate()) {
                    $('#country').prop('disabled', false);

                    $.ajax({
                        method: "POST",
                        url: "{{ route('buyer_add_shipping_address') }}",
                        data: $('#modalForm').serialize(),
                    }).done(function( data ) {
                        window.location.reload(true);
                    });

                    $('#country').prop('disabled', true);
                }
            });

            $('#addEditShippingModal').on('hide.bs.modal', function (event) {
                $("#locationUS").prop("checked", true);
                $('.location').trigger('change');

                $('#store_no').val('');
                $('#address').val('');
                $('#unit').val('');
                $('#city').val('');
                $('#stateSelect').val('');
                $('#state').val('');
                $('#zipCode').val('');
                $('#phone').val('');
                $('#fax').val('');
                $('#showroomCommercial').prop('checked', false);

                clearModalForm();
            });

            function clearModalForm() {
                $('#form-group-address').removeClass('has-danger');
                $('#form-group-city').removeClass('has-danger');
                $('#form-group-state-select').removeClass('has-danger');
                $('#form-group-state').removeClass('has-danger');
                $('#form-group-country').removeClass('has-danger');
                $('#form-group-zip').removeClass('has-danger');
                $('#form-group-phone').removeClass('has-danger');
            }

            // Default Address
            $('.defaultAddress').change(function () {
                var id = $('.defaultAddress:checked').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('buyer_default_shipping_address') }}",
                    data: { id: id },
                }).done(function( data ) {
                    toastr.success('Default Shipping Address Changed!');
                });
            });

            // Delete Address
            var selectedId = '';

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('buyer_delete_shipping_address') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    window.location.reload(true);
                });
            });

            // Edit Shipping Address
            $('.btnEdit').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');
                $('#editAddressId').val(id);

                var address = shippingAddresses[index];

                if (address.location == 'US')
                    $("#locationUS").prop("checked", true);
                else if (address.location == 'CA')
                    $("#locationCA").prop("checked", true);
                else
                    $('#locationInt').prop("checked", true);

                $('.location').trigger('change');

                $('#store_no').val(address.store_no);
                $('#address').val(address.address);
                $('#unit').val(address.unit);
                $('#city').val(address.city);
                $('#stateSelect').val(address.state_id);
                $('#state').val(address.state_text);
                $('#zipCode').val(address.zip);
                $('#country').val(address.country_id);
                $('#phone').val(address.phone);
                $('#fax').val(address.fax);

                if (address.commercial == 1)
                    $('#showroomCommercial').prop('checked', true);

                $('#addEditShippingModal').modal('show');
                $('#modalBtnAdd').hide();
                $('#modalBtnUpdate').show();
            });

            $('#modalBtnUpdate').click(function () {
                if (!shippingAddressValidate()) {
                    $('#country').prop('disabled', false);

                    $.ajax({
                        method: "POST",
                        url: "{{ route('buyer_edit_shipping_address') }}",
                        data: $('#modalForm').serialize(),
                    }).done(function( data ) {
                        window.location.reload(true);
                    });

                    $('#country').prop('disabled', true);
                }
            });

            function shippingAddressValidate() {
                var error = false;
                var location = $('.location:checked').val();

                clearModalForm();

                if ($('#address').val() == '') {
                    $('#form-group-address').addClass('text-danger');
                    $("#form-group-address").prop('required',true);
                    error = true;
                }

                if ($('#city').val() == '') {
                    $('#form-group-city').addClass('text-danger');
                    $("#city").prop('required',true);
                    error = true;
                }

                if ((location == 'US' || location == 'CA') && $('#stateSelect').val() == '') {
                    $('#form-group-state-select').addClass('text-danger');
                    $("#stateSelect").prop('required',true);
                     error = true;
                }

                if (location == 'INT' && $('#state').val() == '') {
                    $('#form-group-state').addClass('text-danger');
                    $("#state").prop('required',true);
                    error = true;
                }

                if ($('#country').val() == '') {
                    $('#form-group-country').addClass('text-danger');
                    $("#country").prop('required',true);
                    error = true;
                }

                if ($('#zipCode').val() == '') {
                    $('#form-group-zip').addClass('text-danger');
                    $("#zipCode").prop('required',true);

                    error = true;
                }

                if ($('#phone').val() == '') {
                    $('#form-group-phone').addClass('text-danger');
                    $("#phone").prop('required',true);
                    error = true;
                }

                return error;
            }
        })
    </script>
@stop