<?php use App\Enumeration\OrderStatus; ?>
@extends('layouts.home_layout')

@section('content')
<!-- =========================
    START BANNER SECTION
============================== -->
<section class="banner_area common_banner clearfix">
    <div class="container container_full_width_mobile">
        <div class="row">
            <div class="col-md-12 custom_padding_9">
                <div class="banner_top">
                    <p>IT’S HERE: THE WEDDING SALE! | <a href="#">SHOP NEW MARKDOWNS</a> </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END BANNER SECTION
============================== -->  

<!-- =========================
    START APPOINMENT SECTION
============================== -->
<section class="appoinment_area common_content_area">
    <div class="container">
        <div class="row">
            <div class="col-md-2 custom_padding_9 for_desktop">
                <div class="common_left_menu">
                    @include('buyer.profile.menu')
                </div>
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-12 custom_padding_9">
                        <div class="my_account_area">
                            
                            <div class="myaccount_title">
                                <h2 class="my_accout_title">My Profile</h2>
                            </div>
                            <div class="my_dashboard_title text-center">
                                <h2>Hello, {{$buyerInfo->first_name.' '.$buyerInfo->last_name}}</h2>
                                <p>Welcome to Maniju Fashion</p>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 my_dasboard_custom_padding">
                                    <div class="my_dashboard_inner text-center my_info_bg_1">
                                        <h2>My Information</h2>
                                        <img src="{{asset('/images/my-account/myacc-ic1.svg')}}" alt="">
                                        <a href="{{route('buyer_my_information')}}">Update My Info</a>
                                    </div>
                                </div>
                                <div class="col-lg-4 my_dasboard_custom_padding">
                                    <div class="my_dashboard_inner text-center my_info_bg_2">
                                        <h2>My Wishlist</h2>
                                        <img src="{{asset('/images/my-account/myacc-ic2.svg')}}" alt="">
                                        <a href="{{ route('view_wishlist') }}">See your wishlist</a>
                                    </div>
                                </div>
                                <div class="col-lg-4 my_dasboard_custom_padding">
                                    <div class="my_dashboard_inner text-center my_info_bg_3">
                                        <h2>Order History</h2>
                                        <img src="{{asset('/images/my-account/myacc-ic3.svg')}}" alt="">
                                        <a href="{{ route('buyer_show_orders') }}">See My Orders</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END APPOINMENT SECTION
============================== -->
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            $('#btnApprove').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('order_reject_status_change') }}",
                    data: { id: id, status: 2 },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            });

            $('#btnDecline').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('order_reject_status_change') }}",
                    data: { id: id, status: 1 },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            });
        });
    </script>
@stop