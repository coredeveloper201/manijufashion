<?php use App\Enumeration\Role; ?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $meta_title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ $meta_description }}"/>
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="{{ asset('themes/front/css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('themes/front/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/front/css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/front/css/main.css') }}?id={{ rand() }}">
    <link rel="stylesheet" href="{{ asset('themes/front/css/custom.css') }}?id={{ rand() }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">
    @yield('additionalCSS')

</head>

<!-- Body-->
<body class="product_page home_page">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header -->
@include('layouts.shared.header')
<!-- Header -->

<!-- Content -->
@yield('content')
<!-- Content -->

<!-- =========================
    START FOOTER SECTION
============================== -->
<footer class="footer_area ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer_top_menu clearfix">
                    <ul>
                        <li><a href="#">Customer Service</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">USA change</a></li>
                        <li><a href="#">Resources</a></li>
                        <li><a href="#">Anthropologie</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Designers</a></li>
                    </ul>
                </div>
                <div class="footer_inner_wrapper clearfix">
                    <div class="footer_inner footer_inner_blog">
                        <a href="#">BHLDN Blog</a>
                    </div>
                    <div class="footer_inner">
                        <ul class="footer_social">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-google"></i></a></li>
                        </ul>
                    </div>
                    <div class="footer_inner footer_sign_up">
                        <input type="text" class="form-control">
                        <label for="">sign up to receive our emails</label>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="footer_long_desc">
                    <h2>About BHLDN</h2>
                    <p>Hi there! Thanks for stopping by. BHLDN (pronounced beholden) is your one-stop shop for all things bridal and event. Brought to you exclusively by Anthropologie, BHLDN Weddings offers a full assortment of wedding dresses, bridesmaid dresses, accessories, and décor for price-conscious brides that still want to WOW. We're here for every aspect of your big day, <a href="#">wedding dresses,</a> <a href="#">bridesmaid dresses,</a> <a href="#">accessories,</a> <a href="#">mother of the bride dresses,</a> <a href="#">wedding gowns, and even special events.</a> With styles from designers <a href="#">Badgley Mischka,</a> <a href="#">Catherine Deane,</a> <a href="#">Donna Morgan,</a> <a href="#">Jenny Yoo,</a> <a href="#">Monique Lhuillier,</a> <a href="#">Needle & Thread,</a> <a href="#">Tadashi Shoji,</a> Whispers & Echoes, Yumi Kim and more, you are sure to find your perfect gown and accessories. Some of our gown styles are a-line, modern ballgowns, lace dresses, and more. Our web stylists are available to answer any styling questions you have, from which delicate earring to wear to whether that bridal sash goes with your wedding dress. Not sure where to start with your bridal party? Check out our introduction to mix & match bridesmaids dresses which will give your BHLDN bridesmaids a chance to shine, and most importantly, feel comfortable. If you are just looking for a <a href="#">wedding guest dress</a> we have a curated assortment of occasion dresses that will make eyes turn at the ceremony. Don't forget to check out our new arrivals, little white dresses, bridal accessories, bridesmaid robes , and bridal separates before you leave! We love our #BHLDNbrides and can't wait to help you celebrate with our BHLDN <a href="#">wedding dresses</a>. Head over to our <a href="#">wedding blog</a> for more inspiration, tips AND advice for all your wedding or event needs.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="footer_bottom_list">
                        <li>© BHLDN LLC 2019 </li>
                        <li><a href="#">TERMS OF USE</a></li>
                        <li><a href="#">PRIVACY POLICY</a></li>
                        <li><a href="#">CA NOTICE</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- =========================
    END FOOTER SECTION
============================== -->

<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="{{ asset('themes/front/js/vendor/jquery-3.3.1.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="{{ asset('themes/front/js/vendor/bootstrap.js') }}"></script>
<script src="{{ asset('themes/front/js/owl.carousel.js') }}"></script>
<script src="{{ asset('themes/front/js/jquery.matchHeight.js') }}"></script>
<script src="{{ asset('themes/front/js/main.js') }}?id={{ rand() }}"></script>
@yield('additionalJS')
</body>
</html>