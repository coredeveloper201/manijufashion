<?php use App\Enumeration\Role; ?>
        <!-- =========================
            START HEADER SECTION
        ============================== -->
<header class="header_area">
    <div class="header_desktop">
        <div class="header_top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header_search">
                            <input type="text" name="googlesearch" placeholder="">
                            <label>Search</label>
                            <button><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="col-md-6">
                        @if(\Auth::check())
                        <div class="header_right">
                            <ul>
                                <li><a href="#">CART</a></li>
                                <li><a href="{{ route('buyer_show_overview') }}">MY ACCOUNT</a></li>
                                <li><a href="{{ route('logout_buyer_get') }}">LOGOUT</a></li>
                                <li><a href="#"><img src="{{ asset('themes/front/images/cart.png') }}" alt=""> <span>0</span></a></li>
                            </ul>
                        </div>
                        @else
                        <div class="header_right">
                            <ul>
                                <li><a href="#">CART</a></li>
                                <li><a href="{{ route('buyer_register') }}">SIGN UP</a></li>
                                <li><a href="{{ route('buyer_login') }}">SIGN IN</a></li>
                                <li><a href="#"><img src="{{ asset('themes/front/images/cart.png') }}" alt=""> <span>0</span></a></li>
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main_logo">
                        <a href="{{ route('home') }}" class="navbar-brand">
                            <img src="{{ $white_logo_path }}" alt="" class="img-responsive white_logo">
                        </a>
                    </div>
                    <nav class="main_menu">

                        <ul>

                            @foreach($default_categories as $cat)
                                <li class="main-menu-li">
                                    <a href="{{ route('category_page', ['category' => changeSpecialChar($cat['name'])]) }}">{{ $cat['name'] }}</a>
                                    <?php
                                    $subIds = [];

                                    foreach ($cat['subCategories'] as $d_sub)
                                        $subIds[] = $d_sub['id'];
                                    ?>
                                    @if(count($cat['subCategories'])>0)
                                    <ul class="dropdown-menu __custom_drop_main">
                                        @foreach($cat['subCategories'] as $d_sub)
                                            <li><a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($cat['name'])]) }}">{{ $d_sub['name'] }}</a></li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>


                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="header_mobile clearfix">
        <div class="header_left_mobile">
            <ul>
                <li>
                    <div class="showhide">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </li>
                <li>
                    <div class="mobile_location">
                        <a href="#"><i class="fas fa-map-marker-alt"></i></a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="mobile_logo">
            <a class="navbar-brand" href="#">
                <img src="{{ $white_logo_path }}" alt="" class="img-responsive white_logo_mobile">
            </a>
        </div>
        <div class="header_right_mobile">
            <ul>
                <li><i class="fas fa-search"></i></li>
                <li><a href="#"><img src="{{ asset('themes/front/images/cart.png') }}" alt=""> <span>0</span></a></li>
            </ul>
        </div>
        <div class="header_search_mobile clearfix">
            <input type="text" class="form-control" placeholder="Search">
        </div>
    </div>

    <div class="mobile_overlay"></div>
    <div class="mobile_menu">
        <div class="top_menu_list clearfix">
            <h2><a href="#">Account</a> <span class="ic_c_mb"><img src="{{ asset('themes/front/images/cross_ic.png') }}" alt=""></span></h2>
        </div>
        <div class="menu-list clearfix">
            <ul id="menu-content" class="menu-content">
                <li><a href="#">Appointments</a></li>
                <li data-toggle="collapse" data-target="#New" class="with_collapse collapsed">
                    <a href="#">New</a>
                </li>
                <ul class="collapse clearfix" id="New">
                    <li data-toggle="collapse" data-target="#ViewAll" class="with_collapse collapsed">
                        <a href="#">View All New </a>
                    </li>
                    <ul class="collapse clearfix" id="ViewAll">
                        <li><a href="#">Wedding Dresses</a></li>
                        <li><a href="#">Dresses</a></li>
                        <li><a href="#">Shoes & Accessories</a></li>
                        <li><a href="#">Lingerie</a></li>
                        <li><a href="#">Décor & Gifts</a></li>
                    </ul>
                </ul>
                <li data-toggle="collapse" data-target="#Bride" class="with_collapse collapsed">
                    <a href="#">Bride</a>
                </li>
                <ul class="collapse clearfix" id="Bride">
                    <li><a href="#">Wedding Dresses</a></li>
                    <li><a href="#">Dresses</a></li>
                    <li><a href="#">Shoes & Accessories</a></li>
                    <li><a href="#">Lingerie</a></li>
                    <li><a href="#">Décor & Gifts</a></li>
                </ul>
                <li data-toggle="collapse" data-target="#Party" class="with_collapse collapsed">
                    <a href="#">Bridesmaids & Bridal Party</a>
                </li>
                <ul class="collapse clearfix" id="Party">
                    <li><a href="#">Wedding Dresses</a></li>
                    <li><a href="#">Dresses</a></li>
                    <li><a href="#">Shoes & Accessories</a></li>
                    <li><a href="#">Lingerie</a></li>
                    <li><a href="#">Décor & Gifts</a></li>
                </ul>
                <li data-toggle="collapse" data-target="#Dresses" class="with_collapse collapsed">
                    <a href="#">Occasion Dresses</a>
                </li>
                <ul class="collapse clearfix" id="Dresses">
                    <li><a href="#">Wedding Dresses</a></li>
                    <li><a href="#">Dresses</a></li>
                    <li><a href="#">Shoes & Accessories</a></li>
                    <li><a href="#">Lingerie</a></li>
                    <li><a href="#">Décor & Gifts</a></li>
                </ul>
                <li data-toggle="collapse" data-target="#Lingerie" class="with_collapse collapsed">
                    <a href="#">Lingerie</a>
                </li>
                <ul class="collapse clearfix" id="Lingerie">
                    <li><a href="#">Wedding Dresses</a></li>
                    <li><a href="#">Dresses</a></li>
                    <li><a href="#">Shoes & Accessories</a></li>
                    <li><a href="#">Lingerie</a></li>
                    <li><a href="#">Décor & Gifts</a></li>
                </ul>
                <li data-toggle="collapse" data-target="#Accessories" class="with_collapse collapsed">
                    <a href="#">Shoes & Accessories</a>
                </li>
                <ul class="collapse clearfix" id="Accessories">
                    <li><a href="#">Wedding Dresses</a></li>
                    <li><a href="#">Dresses</a></li>
                    <li><a href="#">Shoes & Accessories</a></li>
                    <li><a href="#">Lingerie</a></li>
                    <li><a href="#">Décor & Gifts</a></li>
                </ul>
                <li data-toggle="collapse" data-target="#Gifts" class="with_collapse collapsed">
                    <a href="#">Décor & Gifts</a>
                </li>
                <ul class="collapse clearfix" id="Gifts">
                    <li><a href="#">Wedding Dresses</a></li>
                    <li><a href="#">Dresses</a></li>
                    <li><a href="#">Shoes & Accessories</a></li>
                    <li><a href="#">Lingerie</a></li>
                    <li><a href="#">Décor & Gifts</a></li>
                </ul>
                <li data-toggle="collapse" data-target="#Sale" class="with_collapse collapsed">
                    <a href="#">Sale</a>
                </li>
                <ul class="collapse clearfix" id="Sale">
                    <li><a href="#">Wedding Dresses</a></li>
                    <li><a href="#">Dresses</a></li>
                    <li><a href="#">Shoes & Accessories</a></li>
                    <li><a href="#">Lingerie</a></li>
                    <li><a href="#">Décor & Gifts</a></li>
                </ul>
            </ul>
        </div>
    </div>

</header>

<!-- =========================
        END HEADER SECTION
    ============================== -->