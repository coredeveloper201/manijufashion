<?php use App\Enumeration\PageEnumeration; ?>

@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row {{ ($errors && sizeof($errors) > 0) ? 'd-none' : '' }}" id="addBtnRow">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNew">Add New Image</button>
        </div>
    </div>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRow">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitle">Add New Image</span></h3>

            <form class="form-horizontal" enctype="multipart/form-data" id="form" method="post" action="{{ route('admin_top_banner_add') }}">
                @csrf

                <div class="form-group row{{ $errors->has('photo') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="code" class="col-form-label">Image *</label>
                    </div>

                    <div class="col-lg-5">
                        <input class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}"
                               type="file" id="photo" name="photo" accept="image/*">
                    </div>
                </div>

                @if ($errors->has('photo'))
                    <div class="form-control-feedback">{{ $errors->first('photo') }}</div>
                @endif

                <div class="form-group row{{ $errors->has('link') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="link" class="col-form-label">Link *</label>
                    </div>

                    <div class="col-lg-5">
                        <input class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}"
                               type="text" id="link" name="link" value="{{ old('link') }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('page') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="page" class="col-form-label">Page *</label>
                    </div>

                    <div class="col-lg-5">
                        <select class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" name="page">
                            <option value="">Select Page</option>
                            <option value="-1">New Arrival</option>
                            <option value="-2">Best Sellers</option>

                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-default" id="btnCancel">Cancel</button>
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="Add">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Page</th>
                            <th>Banner</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($banners as $banner)
                            <tr>
                                <td>
                                    @if ($banner->page == PageEnumeration::$NEW_ARRIVAL)
                                        New Arrival
                                    @elseif ($banner->page == PageEnumeration::$BEST_SELLER)
                                        Best Seller
                                    @else
                                        {{ $banner->category->name or '' }}
                                    @endif
                                </td>
                                
                                <td>
                                    <img src="{{ asset($banner->image_path) }}" width="200px" height="100px">
                                </td>

                                <td>
                                    <a class="text-danger btnRemove" data-id="{{ $banner->id }}">Remove</a> |
                                    <a class="text-info btnEdit" data-id="{{ $banner->id }}">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>

    <div class="modal fade" id="editModal" role="dialog" aria-labelledby="editModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="editModal">Edit Link URL</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="modal_url">
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-primary" id="modalBtnEdit">Save</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sortable/js/Sortable.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var selectedId;
            var message = '{{ session('message') }}';
            var banners = <?php echo json_encode($banners); ?>;

            if (message != '')
                toastr.success(message);

            $('#btnAddNew').click(function () {
                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
            });

            $('#btnCancel').click(function (e) {
                e.preventDefault();

                $('#addEditRow').addClass('d-none');
                $('#addBtnRow').removeClass('d-none');

                // Clear form
                $('input').removeClass('is-invalid');
                $('.form-group').removeClass('has-danger');
            });

            $('.btnRemove').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_top_banner_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            // Edit
            $('.btnEdit').click(function () {
                var id = parseInt($(this).data('id'));
                selectedId = id;

                $.each(banners, function (i, img) {
                    if (img.id == id)
                        banner = img;
                });

                $('#modal_url').val(banner.url);
                $('#editModal').modal('show');
            });

            $('#modalBtnEdit').click(function () {
                var url = $('#modal_url').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_top_banner_edit_post') }}",
                    data: { id: selectedId, url: url }
                }).done(function( msg ) {
                    location.reload();
                });
            });
        })
    </script>
@stop