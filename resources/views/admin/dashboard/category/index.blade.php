@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/Nestable/style.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddCategory">Add New Category</button>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="dd" id="nestable">
                <ol class="dd-list">
                    @foreach($categories as $category)
                        <li class="dd-item" data-id="{{ $category['id'] }}">
                            <div class="dd-handle dd3-handle">{{ $category['name'] }}</div>
                            <div class="editdelete">
                            <span style="float: right">
                                &nbsp;<a style="color: red" class="btnDelete" data-id="{{ $category['id'] }}">Delete</a>
                            </span>

                                <span style="float: right">
                                <a style="color: blue" class="btnEdit" data-id="{{ $category['id'] }}" data-index="{{ $loop->index }}">Edit</a> |
                            </span>
                            </div>

                            @if (sizeof($category['subCategories']) > 0)
                                <ol class="dd-list">
                                    @foreach($category['subCategories'] as $sub)
                                        <li class="dd-item" data-id="{{ $sub['id'] }}">
                                            <div class="dd-handle dd3-handle">{{ $sub['name'] }}</div>
                                            <div class="editdelete">
                                            <span style="float: right">
                                                &nbsp;<a style="color: red" class="btnDelete" data-id="{{ $sub['id'] }}">Delete</a>
                                            </span>

                                                <span style="float: right">
                                                <a style="color: blue"
                                                   class="btnEdit"
                                                   data-id="{{ $sub['id'] }}"
                                                   data-index="{{ $loop->index }}"
                                                   data-parent="{{ $loop->parent->index }}">Edit
                                                </a> |
                                            </span>
                                            </div>

                                            @if (sizeof($sub['subCategories']) > 0)
                                                <ol class="dd-list">
                                                    @foreach($sub['subCategories'] as $sub2)
                                                        <li class="dd-item" data-id="{{ $sub2['id'] }}">
                                                            <div class="dd-handle dd3-handle">{{ $sub2['name'] }}</div>
                                                            <div class="editdelete">
                                                            <span style="float: right">
                                                                &nbsp;<a style="color: red" class="btnDelete" data-id="{{ $sub2['id'] }}">Delete</a>
                                                            </span>

                                                                <span style="float: right">
                                                                <a style="color: blue"
                                                                   class="btnEdit"
                                                                   data-id="{{ $sub2['id'] }}"
                                                                   data-index="{{ $loop->index }}"
                                                                   data-parent="{{ $loop->parent->parent->index }}"
                                                                   data-secondary-parent="{{ $loop->parent->index }}">Edit
                                                                </a> |
                                                            </span>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            @endif
                                        </li>
                                    @endforeach
                                </ol>
                            @endif
                        </li>
                    @endforeach
                </ol>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addEditModal" role="dialog" aria-labelledby="addEditModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white" id="addEditModal">Add Category</h4>
                </div>

                <div class="modal-body">
                    <form class="form-horizontal">
                        <fieldset>
                            <div class="form-group row">
                                <div class="col-lg-5 text-lg-right">
                                    <label for="modalCategoryName" class="col-form-label">Category Name</label>
                                </div>
                                <div class="col-lg-6">
                                    <input type="text" id="modalCategoryName" class="form-control" placeholder="Category Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-5 text-lg-right">
                                    <label for="modalParent" class="col-form-label">Parent</label>
                                </div>
                                <div class="col-lg-6">
                                    <select class="form-control" id="modalParent">
                                        <option value="0">Select Parent</option>

                                        @foreach($categories as $category)
                                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-5 text-lg-right">
                                    <label for="modalSecondaryParent" class="col-form-label">Secondary Parent</label>
                                </div>
                                <div class="col-lg-6">
                                    <select class="form-control" id="modalSecondaryParent">
                                        <option value="0">Select Secondary Parent</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div class="modal-footer">
                    <button class="btn  btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn  btn-primary" id="modalBtnAddCategory">Add Category</button>
                    <button class="btn  btn-primary" id="modalBtnUpdateCategory">Update Category</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete Category</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/Nestable/jquery.nestable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            var categories = <?php echo json_encode($categories); ?>;
            var selectedID = 0;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#modalBtnAddCategory').click(function () {
                var categoryName = $('#modalCategoryName').val();
                var parentID = $('#modalParent').val();
                var secondaryParentID = $('#modalSecondaryParent').val();

                if (categoryName == '') {
                    $('#modalCategoryName').addClass('is-invalid');
                } else {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_category_add') }}",
                        data: { categoryName: categoryName, parentID: parentID, secondaryParentID: secondaryParentID }
                    }).done(function( msg ) {
                        location.reload();
                    });
                }
            });

            $('#nestable').nestable({
                maxDepth: 3
            }).on('change', '.dd-item', function(e) {
                e.stopPropagation();

                var itemArray = $('.dd').nestable('serialize');

                var id = $(this).data('id'),
                    parentId = $(this).parents('.dd-item').data('id');

                if (parentId == undefined)
                    parentId = 0;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_sort_category') }}",
                    data: { itemArray: itemArray }
                }).done(function( msg ) {
                    toastr.success('Category Updated!');
                });
            });

            $('#btnAddCategory').click(function() {
                $('#modalCategoryName').removeClass('is-invalid');
                $('#modalCategoryName').val('');
                $('#modalParent').val('0');
                $('#modalSecondaryParent').html('<option value="0">Select Secondary Parent</option>');

                $('#modalBtnAddCategory').show();
                $('#modalBtnUpdateCategory').hide();
                $('#addEditModal').modal('show');
            });

            $('#modalParent').change(function () {
                var index = $(this).prop('selectedIndex');

                $('#modalSecondaryParent').html('<option value="0">Select Secondary Parent</option>');

                if (index != 0) {
                    var subCategories = categories[index - 1].subCategories;

                    $.each(subCategories, function (index, value) {
                        $('#modalSecondaryParent').append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                }
            });

            $('.btnDelete').click(function () {
                selectedID = $(this).data('id');
                $('#deleteModal').modal('show');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_category_delete') }}",
                    data: { id: selectedID }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            $('.btnEdit').click(function () {
                selectedID = $(this).data('id');
                var index = $(this).data('index');
                var parent = $(this).data('parent');
                var secondaryParent = $(this).data('secondary-parent');

                if (secondaryParent !== undefined)
                    var category = categories[parent].subCategories[secondaryParent].subCategories[index];
                else if (parent !== undefined)
                    var category = categories[parent].subCategories[index];
                else
                    var category = categories[index];


                $('#modalCategoryName').removeClass('is-invalid');
                $('#modalParent').val('0');
                $('#modalSecondaryParent').html('<option value="0">Select Secondary Parent</option>');

                if (parent !== undefined) {
                    $('#modalParent').val(categories[parent].id);
                    var subCategories = categories[parent].subCategories;
                    var secondaryParentID = 0;

                    if (secondaryParent !== undefined)
                        secondaryParentID = categories[parent].subCategories[secondaryParent].id;

                    $.each(subCategories, function (index, value) {

                        if (secondaryParentID == value.id)
                            $('#modalSecondaryParent').append('<option value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#modalSecondaryParent').append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                }

                $('#modalCategoryName').val(category.name);

                $('#modalBtnAddCategory').hide();
                $('#modalBtnUpdateCategory').show();
                $('#addEditModal').modal('show');
            });

            $('#modalBtnUpdateCategory').click(function () {
                var categoryName = $('#modalCategoryName').val();
                var parentID = $('#modalParent').val();
                var secondaryParentID = $('#modalSecondaryParent').val();

                if (categoryName == '') {
                    $('#modalCategoryName').addClass('is-invalid');
                } else {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_category_update') }}",
                        data: { id: selectedID, categoryName: categoryName, parentID: parentID, secondaryParentID: secondaryParentID }
                    }).done(function( msg ) {
                        location.reload();
                    });
                }
            });
        });
    </script>
@stop