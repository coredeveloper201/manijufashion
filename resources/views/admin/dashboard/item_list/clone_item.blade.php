<?php use App\Enumeration\Availability; ?>
@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/ezdz/jquery.ezdz.min.css') }}" rel="stylesheet">
    <style>
        .block__list_tags li {
            color: #fff;
            float: left;
            margin: 8px 20px 10px 0;
            padding: 5px 22px;
            min-width: 10px;
            text-align: center;
        }
        .layer.block #image-container [draggable="false"],.sortable-chosen{
            background: #0fb0c0;
            padding: 7px 22px;
            border-radius: 4px;
            border: none;

        }

        .block__list {
            max-width: none;
            margin: 0;
            padding:0 10px;
            list-style: none;
            background-color: #fff;
        }

        .block__list li {
            cursor: move;
        }

        .block__list_tags {
            padding-left: 30px;
        }

        .block__list_tags:after {
            clear: both;
            content: '';
            display: block;
        }

        .block__list_tags li:first-child:first-letter {
            text-transform: uppercase;
        }
        .block__list.block__list_words li{
            padding-left: 47px !important;
        }

        .btnRemoveImage {
            color: red !important;
        }

        .ezdz-dropzone {
            height: 80px;
            line-height: 70px;
            border: 5px dashed lightgray;
            border-radius: 10px;
        }
    </style>
@stop

@section('content')
    <form class="form-horizontal" method="post" action="{{ route('admin_clone_item_post', ['old_item' => $item->id]) }}" id="form">
        @csrf

        <div class="global_accordion edit_item_list">
            <div class="container-fluid no-padding">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne">
                                Item Info
                            </button>
                        </div>

                        <div id="collapseOne" class="collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="status" class="col-form-label">Status</label>
                                            </div>

                                            <div class="col-6">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="statusActive" name="status" class="custom-control-input"
                                                           value="1"  {{ empty(old('status')) ? ($item->status == 1 ? 'checked' : '') : (old('status') == 1 ? 'checked' : '') }}>
                                                    <label class="custom-control-label" for="statusActive">Active</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="statusInactive" name="status" class="custom-control-input"
                                                           value="0" {{ empty(old('status')) ? ($item->status == 0 ? 'checked' : '') : (old('status') == 0 ? 'checked' : '') }}>
                                                    <label class="custom-control-label" for="statusInactive">Inactive</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="style_no" class="col-form-label">Style No.*</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="text" id="style_no" class="form-control{{ $errors->has('style_no') ? ' is-invalid' : '' }}"
                                                       name="style_no" value="{{ empty(old('style_no')) ? ($errors->has('style_no') ? '' : $item->style_no.'-Clone') : old('style_no') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="price" class="col-form-label">Price *</label>
                                            </div>

                                            <div class="col-3">
                                                <input type="text" id="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                                                       placeholder="$" name="price" value="{{ empty(old('price')) ? ($errors->has('price') ? '' : $item->price) : old('price') }}">
                                            </div>

                                            <div class="col-2">
                                                <label for="orig_price" class="col-form-label">Orig. Price</label>
                                            </div>

                                            <div class="col-3">
                                                <input type="text" id="orig_price" class="form-control{{ $errors->has('orig_price') ? ' is-invalid' : '' }}"
                                                       placeholder="$" name="orig_price" value="{{ empty(old('orig_price')) ? ($errors->has('orig_price') ? '' : $item->orig_price) : old('orig_price') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="size" class="col-form-label">Size *</label>
                                            </div>

                                            <div class="col-8">
                                                <select class="form-control{{ $errors->has('size') ? ' is-invalid' : '' }}" name="size" id="size">
                                                    <option value="">Select Size</option>

                                                    @foreach($packs as $pack)
                                                        <option value="{{ $pack->id }}" data-index="{{ $loop->index }}"
                                                                {{ empty(old('size')) ? ($errors->has('size') ? '' : ($item->pack_id == $pack->id ? 'selected' : '')) :
                                                                    (old('size') == $pack->id ? 'selected' : '') }}>{{ $pack->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="sorting" class="col-form-label">Sorting #</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="text" id="sorting" class="form-control{{ $errors->has('sorting') ? ' is-invalid' : '' }}"
                                                       name="sorting" value="{{ empty(old('sorting')) ? ($errors->has('sorting') ? '' : $item->sorting) : old('sorting') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="description" class="col-form-label">Description</label>
                                            </div>

                                            <div class="col-8">
                                                <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                                    name="description" rows="4">{{ empty(old('description')) ? ($errors->has('description') ? '' : $item->description) : old('description') }}</textarea>
                                            </div>
                                        </div>

                                {{--        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="guest_image" class="col-form-label">Is it guest image?</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="radio" name="guest_image" value="1" {{ empty(old('guest_image')) ? ($item->guest_image == 1 ? 'checked' : '') : (old('guest_image') == 1 ? 'checked' : '') }}> Yes
                                                <input type="radio" name="guest_image" value="0" {{ empty(old('guest_image')) ? ($item->guest_image == 0 ? 'checked' : '') : (old('guest_image') == 0 ? 'checked' : '') }}> No<br>
                                                <label>Notice: If yes then this item's image will be shown for guest users</label>
                                            </div>
                                        </div>--}}

                                        {{-- <div class="form-group row">
                                            <div class="col-2">
                                                <label for="guest_image" class="col-form-label">Minimum Order</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="number" name="min_order" value="{{ empty(old('min_order')) ? ($errors->has('min_order') ? '' : $item->min_order) : old('min_order') }}" min="10">
                                                <label>Notice: Minimum quantity 10</label>
                                            </div>
                                        </div> --}}
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label class="col-form-label">Available On</label>
                                            </div>

                                            <div class="col-4">
                                                <input type="text" id="available_on" class="form-control{{ $errors->has('available_on') ? ' is-invalid' : '' }}"
                                                       name="available_on"
                                                       value="{{ empty(old('description')) ? ($errors->has('description') ? '' : ($item->available_on != null) ? date('m/d/Y', strtotime($item->available_on)) : '') : old('description') }}">
                                            </div>

                                            <div class="col-4">
                                                <select class="form-control" name="availability" id="availability">
                                                    <option value="{{ Availability::$IN_STOCK }}"
                                                            {{ empty(old('availability')) ? ($errors->has('availability') ? '' : ($item->availability == Availability::$ARRIVES_SOON ? 'selected' : '')) :
                                                                    (old('availability') == Availability::$ARRIVES_SOON ? 'selected' : '') }}>In Stock</option>
                                                    <option value="{{ Availability::$ARRIVES_SOON }}"
                                                            {{ empty(old('availability')) ? ($errors->has('availability') ? '' : ($item->availability == Availability::$ARRIVES_SOON ? 'selected' : '')) :
                                                                    (old('availability') == Availability::$ARRIVES_SOON ? 'selected' : '') }}>Arrives Soon / Back Order</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row{{ $errors->has('item_name') ? ' has-danger' : '' }}">
                                            <div class="col-2">
                                                <label for="item_name" class="col-form-label">Item Name</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="text" id="item_name" class="form-control{{ $errors->has('item_name') ? ' is-invalid' : '' }}"
                                                       name="item_name" value="{{ empty(old('item_name')) ? ($errors->has('item_name') ? '' : $item->name) : old('item_name') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="d_parent_category" class="col-form-label">Default Category *</label>
                                            </div>

                                            <div class="col-3">
                                                <select class="form-control{{ $errors->has('d_parent_category') ? ' is-invalid' : '' }}" name="d_parent_category" id="d_parent_category">
                                                    <option value="">Select Category</option>
                                                    @foreach($defaultCategories as $cat)
                                                        <option value="{{ $cat['id'] }}" data-index="{{ $loop->index }}"
                                                                {{ empty(old('d_parent_category')) ? ($errors->has('d_parent_category') ? '' : ($item->default_parent_category == $cat['id'] ? 'selected' : '')) :
                                                                            (old('d_parent_category') == $cat['id'] ? 'selected' : '') }}>{{ $cat['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-3">
                                                <select class="form-control{{ $errors->has('d_second_parent_category') ? ' is-invalid' : '' }}" name="d_second_parent_category" id="d_second_parent_category">
                                                    <option value="">Sub Category</option>
                                                </select>
                                            </div>

                                            <div class="col-2">
                                                <select class="form-control{{ $errors->has('d_third_parent_category') ? ' is-invalid' : '' }}" name="d_third_parent_category" id="d_third_parent_category">
                                                    <option value="">Sub Category</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label class="col-form-label">Pack</label>
                                            </div>

                                            <div class="col-8">
                                                <label class="col-form-label" id="pack_details">Pack Details</label>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label class="col-form-label">Min. Qty</label>
                                            </div>

                                            <div class="col-2">
                                                <input type="text" id="min_qty" class="form-control{{ $errors->has('min_qty') ? ' is-invalid' : '' }}"
                                                       name="min_qty" value="{{ empty(old('min_qty')) ? ($errors->has('min_qty') ? '' : $item->min_qty) : old('min_qty') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="fabric" class="col-form-label">Fabric</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="text" class="form-control" name="fabric" value="{{ empty(old('fabric')) ? ($errors->has('fabric') ? '' : $item->fabric) : old('fabric') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label class="col-form-label">Details</label>
                                            </div>

                                            <div class="col-4">
                                                <select class="form-control" name="made_n" id="made_n">
                                                    <option value="">Select Made In</option>

                                                    @foreach($madeInCountries as $country)
                                                        <option value="{{ $country->id }}"
                                                                {{ empty(old('made_n')) ? ($errors->has('made_n') ? '' : ($item->made_in_id == $country->id ? 'selected' : '')) :
                                                                            (old('made_n') == $country->id ? 'selected' : '') }}>{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="col-4">
                                                <select class="form-control" name="labeled" id="labeled">
                                                    <option value="">Select Labeled</option>
                                                    <option value="labeled" {{ empty(old('labeled')) ? ($errors->has('labeled') ? '' : ($item->labeled == 'labeled' ? 'selected' : '')) :
                                                    (old('labeled') == 'labeled' ? 'selected' : '') }}>Labeled</option>
                                                    <option value="printed" {{ empty(old('labeled')) ? ($errors->has('labeled') ? '' : ($item->labeled == 'printed' ? 'selected' : '')) :
                                                    (old('labeled') == 'printed' ? 'selected' : '') }}>Printed</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="memo" class="col-form-label">Memo</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="text" id="memo" class="form-control{{ $errors->has('memo') ? ' is-invalid' : '' }}"
                                                       placeholder="Internal use only" name="memo" value="{{ empty(old('memo')) ? ($errors->has('memo') ? '' : $item->memo) : old('memo') }}">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <div class="col-2">
                                                <label for="youtube_url" class="col-form-label">Youtube video ID</label>
                                            </div>

                                            <div class="col-8">
                                                <input type="text" id="youtube_url" class="form-control{{ $errors->has('youtube_url') ? ' is-invalid' : '' }}"
                                                    name="youtube_url" value="{{ empty(old('youtube_url')) ? ($errors->has('youtube_url') ? '' : $item->youtube_url) : old('youtube_url') }}">
                                                    <label>Example: https://youtu.be/ [ ]</label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour">
                            Video
                        </button>
                    </div>
                    <div id="collapseFour" class="collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <input type="file" class="form-control" name="video" id="input-video">

                                    @if ($errors->has('video'))
                                        <span class="text-danger">{{ $errors->first('video') }}</span>
                                    @endif
                                </div>

                                <div class="col-4">
                                    @if ($item->video)
                                        <video height="100" controls autoplay>
                                            <source src="{{ asset($item->video) }}" type="video/mp4">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo">
                            Colors
                        </button>
                    </div>
                    <div id="collapseTwo" class="collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-2">
                                            <input class="form-control" type="text" id="color_search" placeholder="Type Color">

                                            @if ($errors->has('colors'))
                                                <span class="text-danger">Color(s) is required.</span>
                                            @endif
                                        </div>

                                        <select class="col-2 form-control d-none" id="select_master_color">
                                            <option value="">Select Master Color</option>
                                            @foreach($masterColors as $mc)
                                                <option value="{{ $mc->id }}">{{ $mc->name }}</option>
                                            @endforeach
                                        </select>

                                        <div class="col-6">
                                            <a class="btn btn-secondary" role="button" id="btnAddColor">Add Color</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <ul class="colors-ul">
                                                <?php
                                                $previous_color_ids = [];
                                                $available_color_ids = [];

                                                foreach ($item->colors as $t) {
                                                    if ($t->pivot->available == 1)
                                                        $available_color_ids[] = $t->id;
                                                }

                                                if (empty(old('colors'))) {
                                                    foreach($item->colors as $c)
                                                        $previous_color_ids[] = $c->id;
                                                } else {
                                                    $previous_color_ids = old('colors');
                                                }
                                                ?>

                                                @foreach($colors as $color)
                                                    @if (in_array($color->id, $previous_color_ids))
                                                        <li>
                                                            <div class="input-group">
                                                                <div class="form-check custom_checkbox">
                                                                    <input class="form-check-input"
                                                                           type="checkbox"
                                                                           value="1"
                                                                           name="color_available_{{ $color->id }}"
                                                                           id="color_available_{{ $color->id }}"
                                                                            {{ in_array($color->id, $available_color_ids) ? 'checked' : '' }}>
                                                                    <label class="form-check-label color-available" for="color_available_{{ $color->id }}">
                                                                        <span class="name">{{ $color->name }}</span>
                                                                    </label>

                                                                    <a class="color-remove">X</a>
                                                                </div>
                                                            </div>
                                                            <input class="templateColor" type="hidden" name="colors[]" value="{{ $color->id }}">
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseThree">
                            Images
                        </button>
                    </div>
                    <div id="collapseThree" class="collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <button class="btn btn-primary" id="btnUploadImages">Upload Images</button>
                                    <input type="file" class="d-none" multiple id="inputImages">
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col-12">
                                    <ul id="image-container" class="block__list block__list_tags">
                                        @if (old('imagesId') != null && sizeof(old('imagesId')) > 0)
                                            @foreach(old('imagesId') as $img)
                                                <li>
                                                    <div class="image-item" style="margin-right: 10px">
                                                        <img height="150px" width="100px" class="img-thumbnail img" style="margin-bottom: 10px"
                                                             src="{{ asset(old('imagesSrc.'.$loop->index)) }}"><br>
                                                        <select class="image-color" name="imageColor[]">
                                                            <option value="">Color [Default]</option>
                                                        </select><br>
                                                        <a class="btnRemoveImage">Remove</a>

                                                        <input class="inputImageId" type="hidden" name="imagesId[]" value="{{ $img }}">
                                                        <input class="inputImageSrc" type="hidden" name="imagesSrc[]" value="{{ old('imagesSrc.'.$loop->index) }}">
                                                    </div>
                                                </li>
                                            @endforeach
                                        @else
                                            @if (sizeof($item->images) > 0)
                                                @foreach($item->images as $img)
                                                    <li>
                                                        <div class="image-item" style="margin-right: 10px">
                                                            <img height="150px" width="100px" class="img-thumbnail img" style="margin-bottom: 10px"
                                                                 src="{{ asset($img->image_path) }}"><br>
                                                            <select class="image-color" name="imageColor[]">
                                                                <option value="">Color [Default]</option>
                                                            </select><br>
                                                            <a class="btnRemoveImage">Remove</a>

                                                            <input class="inputImageId" type="hidden" name="imagesId[]" value="{{ $img->id }}">
                                                            <input class="inputImageSrc" type="hidden" name="imagesSrc[]" value="{{ $img->image_path }}">
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        @endif
                                    </ul>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div style="width: 100%; height: auto; border: 1px solid black; ">
                                        <div class="row" id="images" style="padding: 20px; text-align: center;">
                                            Drag & Drop Images from your computer
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-right">
                <button class="btn btn-primary" id="btnSave">Save</button>
            </div>
        </div>
    </form>

    <template id="imageTemplate">
        <li>
            <div class="image-item" style="margin-right: 10px">
                <img height="150px" width="100px" class="img-thumbnail img" style="margin-bottom: 10px"><br>
                <select class="image-color" name="imageColor[]">
                    <option value="">Color [Default]</option>
                </select><br>
                <a class="btnRemoveImage">Remove</a>

                <input class="inputImageId" type="hidden" name="imagesId[]">
                <input class="inputImageSrc" type="hidden" name="imagesSrc[]">
            </div>
        </li>
    </template>

    <template id="colorItemTemplate">
        <li>
            <div class="input-group">
                <div class="form-check custom_checkbox">
                    <input class="form-check-input"
                           type="checkbox"
                           value="1" checked>
                    <label class="form-check-label color-available">
                        <span class="name"></span>
                    </label>
                </div>

                <a class="color-remove">X</a>
            </div>
            <input class="templateColor" type="hidden" name="colors[]" value="">
        </li>
    </template>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sortable/js/Sortable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/ezdz/jquery.ezdz.min.js') }}"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
    <script>
        $(function () {
            var defaultCategories = <?php echo json_encode($defaultCategories); ?>;
            var packs = <?php echo json_encode($packs->toArray()); ?>;
            var colors = <?php echo json_encode($colors->toArray()); ?>;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Video
            $('#input-video').ezdz({
                previewImage: false
            });

            // Color select
            var availableColors = [];

            $.each(colors, function (i, color) {
                availableColors.push(color.name);
            });

            $('#color_search').autocomplete({
                source: function (request, response) {
                    var results = $.map(availableColors, function (tag) {
                        if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                            return tag;
                        }
                    });
                    response(results);
                },
                response: function(event, ui) {
                    if (ui.content.length === 0) {
                        $('#select_master_color').val('');
                        $('#select_master_color').removeClass('d-none');
                    } else {
                        $('#select_master_color').addClass('d-none');
                    }
                }
            });

            $('#color_search').keydown(function (e){
                if(e.keyCode == 13){
                    e.preventDefault();
                    addColor();
                }
            });

            $('#color_search').keyup(function (e) {
                if ($('#color_search').val().length == 0)
                    $('#select_master_color').addClass('d-none');
            });

            $('#btnAddColor').click(function () {
                if ($('#select_master_color').hasClass('d-none')) {
                    addColor();
                } else {
                    var id = $('#select_master_color').val();
                    var name = $('#color_search').val();

                    if (id == '')
                        return alert('Select Master Color.');

                    if (name == '')
                        return alert('Enter color name.');

                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_item_add_color') }}",
                        data: { id: id, name: name }
                    }).done(function( data ) {
                        if (data.success) {
                            availableColors.push(data.color.name);
                            colors.push(data.color);

                            $('#select_master_color').addClass('d-none');
                            $('#color_search').val(data.color.name);
                            addColor();
                        } else {
                            alert(data.message);
                        }
                    });
                }
            });

            function addColor() {
                var text = $('#color_search').val();

                if (text != '') {
                    var color = '';

                    $.each(colors, function (i, c) {
                        if (c.name == text)
                            color = c;
                    });

                    if (color != '') {
                        var found = false;
                        $( "input[name*='colors']" ).each(function () {
                            if ($(this).val() == color.id)
                                found = true;
                        });

                        if (!found) {
                            var html = $('#colorItemTemplate').html();
                            row = $(html);

                            row.find('.name').html(color.name);
                            row.find('.templateColor').val(color.id);
                            row.find('.color-available').attr('name', 'color_available_'+color.id);
                            row.find('.color-available').attr('id', 'color_available_'+color.id);
                            row.find('.custom-checkbox').attr('for', 'color_available_'+color.id);

                            $('.colors-ul').append(row);
                            updateImageColors();
                        }
                        $('#color_search').val('');
                    } else {
                        $('#select_master_color').removeClass('d-none');
                    }
                }
            }

            var old_colors = <?php echo json_encode(old('imageColor')); ?>;
            if (old_colors == null)
                old_colors = <?php echo json_encode($imagesColorIds); ?>;

            function updateImageColors() {
                var ids = [];

                $( "input[name*='colors']" ).each(function () {
                    ids.push($(this).val());
                });

                $('.image-color').each(function (i) {
                    var selected = $(this).val();

                    $(this).html('<option value="">Color [Default]</option>');
                    $this = $(this);

                    $.each(ids, function (index, id) {
                        var color = colors.filter(function( obj ) {
                            return obj.id == id;
                        });
                        color = color[0];

                        if ((old_colors.length > i && old_colors[i] == color.id) || color.id == selected)
                            $this.append('<option value="'+color.id+'" selected>'+color.name+'</option>');
                        else
                            $this.append('<option value="'+color.id+'">'+color.name+'</option>');
                    });
                });
            }
            updateImageColors();

            $(document).on('click', '.color-remove', function () {
                $(this).closest('li').remove();
            });

            // Available on
            $('#available_on').datepicker({
                autoclose: true,
                format: 'mm/dd/yyyy'
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            // Images
            var el = document.getElementById('image-container');
            Sortable.create(el, {
                group: "words",
                animation: 150,
            });

            $('#images').on({
                'dragover dragenter': function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                },
                'drop': function(e) {
                    var dataTransfer =  e.originalEvent.dataTransfer;
                    if( dataTransfer && dataTransfer.files.length) {
                        e.preventDefault();
                        e.stopPropagation();
                        $.each( dataTransfer.files, function(i, file) {
                            if (file.size > 2097152) {
                                alert('Max allowed image size is 2MB per image.')
                            } else if (file.type != 'image/jpeg' && file.type != 'image/png') {
                                alert('Only jpg and png file allowed.');
                            } else if ($(".image-container").length > 2) {
                                alert('Maximum 20 photos allows');
                            } else {
                                var xmlHttpRequest = new XMLHttpRequest();
                                xmlHttpRequest.open("POST", '{{ route('admin_item_upload_image') }}', true);
                                var formData = new FormData();
                                formData.append("file", file);
                                xmlHttpRequest.send(formData);

                                xmlHttpRequest.onreadystatechange = function() {
                                    if (xmlHttpRequest.readyState == XMLHttpRequest.DONE) {
                                        var response = JSON.parse(xmlHttpRequest.responseText);

                                        if (response.success) {
                                            var html = $('#imageTemplate').html();
                                            var item = $(html);
                                            item.find('.img').attr('src', response.data.fullPath);
                                            item.find('.inputImageId').val(response.data.id);
                                            item.find('.inputImageSrc').val(response.data.image_path);

                                            $('#image-container').append(item);
                                            updateImageColors();
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            });

            $('body').on('click', '.btnRemoveImage', function () {
                var index = $('.btnRemoveImage').index($(this));
                old_colors.splice(index, 1);
                $(this).closest('li').remove();
            });

            var d_parent_index;
            var d_second_id = '{{ empty(old('d_second_parent_category')) ? ($errors->has('d_second_parent_category') ? '' : $item->default_second_category) : old('d_second_parent_category') }}';
            var d_third_id = '{{ empty(old('d_third_parent_category')) ? ($errors->has('d_third_parent_category') ? '' : $item->default_third_category) : old('d_third_parent_category') }}';

            $('#d_parent_category').change(function () {
                $('#d_second_parent_category').html('<option value="">Sub Category</option>');
                $('#d_third_parent_category').html('<option value="">Sub Category</option>');
                var parent_id = $(this).val();

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').data('index');
                    d_parent_index = index;

                    var childrens = defaultCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        if (value.id == d_second_id)
                            $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#d_second_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }

                $('#d_second_parent_category').trigger('change');
            });

            $('#d_parent_category').trigger('change');

            $('#d_second_parent_category').change(function () {
                $('#d_third_parent_category').html('<option value="">Sub Category</option>');

                if ($(this).val() != '') {
                    var index = $(this).find(':selected').attr('data-index');

                    var childrens = defaultCategories[d_parent_index].subCategories[index].subCategories;

                    $.each(childrens, function (index, value) {
                        if (value.id == d_third_id)
                            $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '" selected>' + value.name + '</option>');
                        else
                            $('#d_third_parent_category').append('<option data-index="' + index + '" value="' + value.id + '">' + value.name + '</option>');
                    });
                }

                var id = $(this).val();
            });

            // Size
            $('#size').change(function () {
                var index = $(this).find(':selected').data('index');

                if (typeof index !== "undefined") {
                    var pack = packs[index];
                    var packDetails = pack.pack1;

                    if (pack.pack2 != null)
                        packDetails += '-' + pack.pack2;

                    if (pack.pack3 != null)
                        packDetails += '-' + pack.pack3;

                    if (pack.pack4 != null)
                        packDetails += '-' + pack.pack4;

                    if (pack.pack5 != null)
                        packDetails += '-' + pack.pack5;

                    if (pack.pack6 != null)
                        packDetails += '-' + pack.pack6;

                    if (pack.pack7 != null)
                        packDetails += '-' + pack.pack7;

                    if (pack.pack8 != null)
                        packDetails += '-' + pack.pack8;

                    if (pack.pack9 != null)
                        packDetails += '-' + pack.pack9;

                    if (pack.pack10 != null)
                        packDetails += '-' + pack.pack10;

                    $('#pack_details').html(packDetails);
                } else {
                    $('#pack_details').html('Pack Details');
                }
            });

            $('#size').trigger('change');
            $('#d_second_parent_category').trigger('change');

            // Upload images button
            $('#btnUploadImages').click(function (e) {
                e.preventDefault();

                $('#inputImages').click();
            });

            $('#inputImages').change(function (e) {
                //console.log(e.target.files);
                $.each(e.target.files, function (index, file) {
                    if (file.size > 2097152) {
                        alert('Max allowed image size is 2MB per image.')
                    } else if (file.type != 'image/jpeg' && file.type != 'image/png') {
                        alert('Only jpg and png file allowed.');
                    } else if ($(".image-container").length > 2) {
                        alert('Maximum 20 photos allows');
                    } else {
                        var xmlHttpRequest = new XMLHttpRequest();
                        xmlHttpRequest.open("POST", '{{ route('admin_item_upload_image') }}', true);
                        var formData = new FormData();
                        formData.append("file", file);
                        xmlHttpRequest.send(formData);

                        xmlHttpRequest.onreadystatechange = function() {
                            if (xmlHttpRequest.readyState == XMLHttpRequest.DONE) {
                                var response = JSON.parse(xmlHttpRequest.responseText);

                                if (response.success) {
                                    // if ($(".image-container").length == 0)
                                    //     $('#images').html('');

                                    var html = $('#imageTemplate').html();
                                    var item = $(html);
                                    item.find('.img').attr('src', response.data.fullPath);
                                    item.find('.inputImageId').val(response.data.id);
                                    item.find('.inputImageSrc').val(response.data.image_path);

                                    $('#image-container').append(item);
                                    updateImageColors();
                                }
                            }
                        }
                    }
                });

                $(this).val('');
            });

            $('#btnSave').click(function (e) {
                e.preventDefault();

                $('#form').submit();
                /*var style_no = $('#style_no').val();
                var current_item_id = '{{ $item->id }}';

                $.ajax({
                    method: "POST",
                    url: "#",
                    data: { style_no: style_no }
                }).done(function( data ) {
                    if (data.success)
                        $('#form').submit();
                    else
                        alert(data.message);
                });*/
            });

            // Available
            $('#available_on').change(function () {
                var val = $(this).val();
                $('#availability').prop('disabled', false);

                if (val != '') {
                    if (moment(val, 'MM/DD/YYYY').isBefore(moment())) {
                        //console.log('before');
                    }else {
                        console.log('after');
                        $('#availability').val('{{ Availability::$ARRIVES_SOON }}');
                        $('#availability').prop('disabled', true);
                    }
                } else {
                    $('#availability').val('{{ Availability::$IN_STOCK }}');
                }
            });

            $('#available_on').trigger('change');

            window.addEventListener("dragover",function(e){
                e = e || event;
                e.preventDefault();
            },false);
            window.addEventListener("drop",function(e){
                e = e || event;
                e.preventDefault();
            },false);
        });
    </script>
@stop