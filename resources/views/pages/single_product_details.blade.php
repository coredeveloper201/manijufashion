<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>

@extends('layouts.home_layout')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fotorama.css') }}">
    <style>
        .color-selected {
            border: 1px solid black !important;
        }
        .sub_category_menu
        {
            margin-left: 0px;
            padding-left: 5px;
            border-left: 1px solid #eee;
        }
        .text_header_txt
        {
            font-size: 14px;
            margin-bottom: 5px;
            font-weight: bold;
            color: #333;
        }
        .form-control {
    display: block;
    width: 100%;
    height: calc(2.25rem + -6px);
    padding: 3px;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}
    </style>
@stop

@section('content')
    <div class="container">
         <!-- =========================
            START BANNER SECTION
        ============================== -->
        <section class="banner_area common_banner clearfix">
            <div class="container container_full_width_mobile">
                <div class="row">
                    <div class="col-md-12 custom_padding_9">
                        <div class="banner_top">
                            <p>IT’S HERE: THE WEDDING SALE! | <a href="#">SHOP NEW MARKDOWNS</a> </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END BANNER SECTION
        ============================== -->
        <!-- =========================
            START BREDCRUMS SECTION
        ============================== -->
        <section class="breadcrumbs_area">
            <div class="container container_full_width_mobile">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active">@php echo $simgleProductDetails[0]->name; @endphp</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            START BREDCRUMS SECTION
        ============================== -->

        <!-- ===============================
            START PRODUCT THUMBNAIL SECTION
        =================================== -->
        <section class="product_single_area">
            <div class="container single_product_container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="single_product_slide">
                            <div id="single_product" class="owl-carousel owl-theme">
                                @foreach($simgleProductDetails[0]->images as $images)
                                 <div class="single_product_slide_inner">
                                    <a href="#">
                                        <img src="{{URL::to('/')}}/{{$images->thumbs_image_path}}" alt="" class="img-fluid">
                                    </a>
                                </div>
                                @endforeach
                            </div>
                            <ul class="single_product_social">
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single_product_description">
                            <h2>@php echo $simgleProductDetails[0]->name; @endphp</h2>
                            <p class="product_price">Price: $@php echo $simgleProductDetails[0]->price; @endphp</p>
                            <p class="text_header_txt">Style No: @php echo $simgleProductDetails[0]->style_no; @endphp</p>
                            <p class="text_header_txt">Fabric: @php echo $simgleProductDetails[0]->fabric; @endphp</p>
                            <p class="text_header_txt">@php echo $simgleProductDetails[0]->description; @endphp</p>
                            <div class="single_product_table">
                                @php
                                $pack_array = array();
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack1);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack2);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack3);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack4);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack5);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack6);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack7);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack8);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack9);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack10);
                                $pack_array = array_values(array_filter($pack_array));
                                @endphp
                                <table id='buy_info_table' class="table">
                                    <thead>
                                        <tr>
                                            <th width="35%">Color</th>
                                            <th style="text-align: center;">{{$simgleProductDetails[0]->pack->name}}</th>
                                            <th width="25%">Pack</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($simgleProductDetails[0]->colors as $colors)
                                        <tr>
                                            <td>{{$colors->name}}</td>
                                            <td style="text-align: center;">
                                                @foreach($pack_array as $pack)
                                                    {{ $pack }}
                                                @endforeach
                                            </td>
                                            <td>
                                                <input type="text" name="" value="" class="form-control" onkeyup="change_pack_details(this)">
                                            </td>
                                            <td>0</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div
                            
                            <p><button class="add_cart_btn">add to cart</button></p>
                            <div class="common_accordion single_product_accordion">
                                <div id="INFORMATION">
                                    <div class="card">
                                        <div class="card-header">
                                            <button class="btn-link collapsed" data-toggle="collapse" data-target="#INFO" >
                                                  Description
                                            </button>
                                        </div>
                                        <div id="INFO" class="collapse">
                                            <div class="card-body clearfix">
                                                <p>Textural appliques adorn the bodice and float down through the full skirt, crafted from layers of glitter tulle. A plunge neckline adds a sultry edge to the whimsical look, and a removable grosgrain belt defines the waist.</p>
                                                <ul>
                                                    <li>By <a href="#">BHLDN</a></li>
                                                    <li>Style #51322717</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <button class="btn-link collapsed" data-toggle="collapse" data-target="#INFORMATIONOne">
                                                  Fit Information
                                            </button>
                                        </div>
                                        <div id="INFORMATIONOne" class="collapse">
                                            <div class="card-body clearfix">
                                                <ul>
                                                    <li>See Size Guide measurements for detailed sizing information</li>
                                                    <li>Fits true to size; take your normal size.</li>
                                                    <li>Fitted at the bust and waist, loose at the hip</li>
                                                    <li>Not bra friendly; additional bust support can be added by an expert bridal tailor</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <button class="btn-link collapsed" data-toggle="collapse" data-target="#Product">
                                                  Product Details
                                            </button>
                                        </div>
                                        <div id="Product" class="collapse">
                                            <div class="card-body clearfix">
                                                <ul>
                                                    <li>Back zip</li>
                                                    <li>Bust cups</li>
                                                    <li>Polyester; polyester lining</li>
                                                    <li>Professionally clean</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                            
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 order-md-first no-padding">
                        <ul class="single_product_left_thumbnail">
                            @foreach($simgleProductDetails[0]->images as $images)
                            <li>
                                <img src="{{URL::to('/')}}/{{$images->thumbs_image_path}}" class="img-fluid" alt="12121">
                             </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- ===============================
            END PRODUCT THUMBNAIL SECTION
        =================================== -->

        <div class="single_link_wrapper">
            <div class="container single_product_container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="appoinment_link store_link single_link text-center">
                                <p>No store near you?</p>
                                <a href="#">See if we have a special event coming to your area!</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>

        <div class="related_item">
            <div class="container single_product_container">
                <div class="row">
                    <div class="col-md-12 custom_padding_5">
                        <div class="related_item_title">
                            <h2>Complete The Look</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-md-4 col-lg-2 custom_padding_5">
                        <div class="product_inner text-center">
                            <a href="#">
                                <img src="" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 custom_padding_5">
                        <div class="product_inner text-center">
                            <a href="#">
                                <img src="" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="related_item">
            <div class="container single_product_container">
                <div class="row">
                    <div class="col-md-12 custom_padding_5">
                        <div class="related_item_title">
                            <h2>You May Also Like</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-md-4 col-lg-2 custom_padding_5">
                        <div class="product_inner text-center">
                            <a href="#">
                                <img src="" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 custom_padding_5">
                        <div class="product_inner text-center">
                            <a href="#">
                                <img src="" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 custom_padding_5">
                        <div class="product_inner text-center">
                            <a href="#">
                                <img src="" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 custom_padding_5">
                        <div class="product_inner text-center">
                            <a href="#">
                                <img src="" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 custom_padding_5">
                        <div class="product_inner text-center">
                            <a href="#">
                                <img src="" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- =========================
            START SINGLE PRODUCT TABLE
        ============================== -->
        <div class="related_item">
            <div class="container single_product_container">
                <div class="row">
                    <div class="col-md-12 custom_padding_5">
                        <div class="related_item_title">
                            <h2>Size Guide</h2>
                            <p>The measurements shown on the Size Guide below are body measurements. Learn how to take your body measurements in the video located under the Size Guide. Once you have these measurements, refer to the Size Guide and Fit Notes at the top of the page to determine which size to purchase.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="single_product_table">
                            <table class="fit-guide"> 
                                <thead> 
                                <tr> 
                                    <th>Size</th>
                                    <th>Bust</th>
                                    <th>Natural Waist</th>
                                    <th>Hip</th>
                                    <th>Length from Hollow to Hem</th>
                                </tr> 
                                </thead> 
                                <tbody> 
                                    <tr>
                                        <td>00</td>
                                        <td>32"</td>
                                        <td>23.5"</td>
                                        <td>35.5"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>0</td>
                                        <td>33"</td>
                                        <td>24.5"</td>
                                        <td>36.5"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>34"</td>
                                        <td>25.5"</td>
                                        <td>37.5"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>35"</td>
                                        <td>26.5"</td>
                                        <td>38.5"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>36.5"</td>
                                        <td>28"</td>
                                        <td>40"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>38"</td>
                                        <td>30"</td>
                                        <td>41.5"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>39.5"</td>
                                        <td>31.5"</td>
                                        <td>43"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>41"</td>
                                        <td>33"</td>
                                        <td>44.5"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>14</td>
                                        <td>42.5"</td>
                                        <td>35.5"</td>
                                        <td>46"</td>
                                        <td>59"</td>
                                    </tr>
                                    <tr>
                                        <td>16</td>
                                        <td>44.5"</td>
                                        <td>36.5"</td>
                                        <td>47.5"</td>
                                        <td>59"</td>
                                    </tr>
                                </tbody> 
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single_link_wrapper no-border">
            <div class="container single_product_container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="appoinment_link store_link single_link text-center">
                                <a href="#">View our "How to Measure" Video</a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script>
        <?php
            echo "var pack_array = ".json_encode($pack_array)." ;";
        ?>

        var total=0;
        for(var i in pack_array)
        { 
            total += parseInt(pack_array[i]); 
        }

        function change_pack_details(obj)
        {
            if($(obj).val() == '')
            {
                $(obj).parent().parent().children('td').eq(3).html('0');
            }
            else
            {
                var input_val = parseInt($(obj).val());
                $(obj).parent().parent().children('td').eq(3).html(total * input_val);
            }            
        }
        $('.add_cart_btn').click(function()
        {
            var table_tr = $('#buy_info_table').children('tbody').children('tr');
            for(var i=0;i<table_tr.length;i++)
            {
                if(table_tr.eq(i).find('input').eq(0).val() == '')
                {
                    table_tr.eq(i).find('input').eq(0).attr('title','Enter pack quantity');
                    table_tr.eq(i).find('input').eq(0).tooltip();
                    table_tr.eq(i).find('input').eq(0).focus();
                    return;
                }
                else
                {

                }
            }
        });
    </script>
@stop