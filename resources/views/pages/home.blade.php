<?php use App\Enumeration\Role; ?>
@extends('layouts.home_layout')
@section('additionalCSS')
@stop
@section('content')
<!-- =========================
START BANNER SECTION
============================== -->
@if(isset($mainSliderImages) && $mainSliderImages[0])
<section class="banner_area clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner_holder">
                    <div class="banner_inner">
                        @if(strpos($mainSliderImages[0]->image_path, '.mp4') !== false)
                            <video loop muted preload="metadata">
                                <source src="{{ asset($mainSliderImages[0]->image_path) }}" type="video/mp4">
                            </video>
                        @else
                            <a href="{{$mainSliderImages[0]->url}}">
                                <img class="img-fluid"  src="{{ asset($mainSliderImages[0]->image_path) }}"  alt="">
                            </a>
                        @endif

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endif

<!-- =========================
END BANNER SECTION
============================== -->

<!-- =========================
START WELCOME SECTION
============================== -->
<section class="welcome_area">
    <div class="container">
        <div class="row">
            <div class="col-md-4 custom_padding_12_half">
                <div class="welcome_inner">
                    <a href="#">
                        <img class="img-fluid" src="{{ asset('themes/front/images/welcom-1.jpg') }}" alt="">
                        <span>Ballgowns</span>
                    </a>
                </div>
            </div>
            <div class="col-md-4 custom_padding_12_half">
                <div class="welcome_inner">
                    <a href="#">
                        <img class="img-fluid" src="{{ asset('themes/front/images/welcom-2.jpg') }}" alt="">
                        <span>Modern Looks</span>
                    </a>
                </div>
            </div>
            <div class="col-md-4 custom_padding_12_half">
                <div class="welcome_inner">
                    <a href="#">
                        <img class="img-fluid" src="{{ asset('themes/front/images/welcom-3.jpg') }}" alt="">
                        <span>Bridal Accessories</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
END WELCOME SECTION
============================== -->

<!-- =========================
START HOME VIDEO SECTION
============================== -->
<section class="home_video_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="home_video_inner">
                    <h2><span>Trend Alert:</span> <br> Bridesmaids in White</h2>

                    <div class="videoWrapper">
                        <!-- Copy & Pasted from YouTube -->
                        <iframe width="560" height="349"
                                src="https://player.vimeo.com/video/312795637?color=e0b2aa&title=0&byline=0&portrait=0&quality=720p"
                                frameborder="0" allowfullscreen></iframe>
                    </div>
                    <h3>Dare to break the rules in our favorite wedding party trend for 2019.</h3>
                    <a href="#">Shop Ivory Bridesmaid <span>Dresses</span> </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
END HOME VIDEO SECTION
============================== -->

<!-- =========================
START HOME PRODUCT SLIDER SECTION
============================== -->
<section class="product_slider_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Dreaming of color?</h2>

                <div id="product_slider1" class="common_slide owl-carousel owl-theme">
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-1.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-2.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-3.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-4.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-5.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-6.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-7.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-8.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="product_slide_link text-center">
                    <a href="#">More shades this way</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
END HOME PRODUCT SLIDER SECTION
============================== -->

<!-- =========================
START PRODUCT WITH TEXT SECTION
============================== -->
<section class="product_with_text_area">
    <div class="container">
        <div class="row">

            <div class="col-md-6 custom_padding_6">
                <div class="product_with_text_wrapper">
                    <a href="#">
                        <img src="{{ asset('themes/front/images/product/product-10.jpg') }}" alt="" class="img-fluid">
                    </a>

                    <div class="product_with_text_inner">
                        <h2 class="text_black">VISIT <br>A BHLDN <br>SHOP</h2>

                        <div class="for_desktop">
                            <p>19 locations. Expert stylists. Happy tears.</p>

                            <p><a href="#">Shop Occasion</a></p>

                            <p><a href="#">See our appointment guide</a></p>
                        </div>
                    </div>
                </div>
                <div class="product_with_text_inner_mobile for_mobile text-center">
                    <p>19 locations. Expert stylists. Happy tears.</p>

                    <p><a href="#">Shop Occasion</a></p>

                    <p><a href="#">See our appointment guide</a></p>
                </div>
            </div>
            <div class="order-md-first col-md-6 custom_padding_6">
                <div class="product_with_text_wrapper">
                    <a href="#">
                        <img src="{{ asset('themes/front/images/product/product-9.jpg') }}" alt="" class="img-fluid">
                    </a>

                    <div class="product_with_text_inner product_with_text_inner_center">
                        <h2>NEW FOR <br>GUESTS</h2>
                        <a href="#">Shop Occasion</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 custom_padding_6">
                <div class="product_with_text_wrapper">
                    <a href="#">
                        <img src="{{ asset('themes/front/images/product/product-11.jpg') }}" alt="" class="img-fluid">
                    </a>

                    <div class="product_with_text_inner product_with_text_inner_top">
                        <h2 class="text_black">GET <br> INSPIRED</h2>

                        <div class="for_desktop">
                            <p>We partnered with <span>Terrain Events</span> to plan an <br> entire wedding from florals
                                to bridal party.</p>
                            <a href="#">See the moodboard</a>
                        </div>
                    </div>
                </div>
                <div class="product_with_text_inner_mobile for_mobile text-center">
                    <p>We partnered with <span>Terrain Events</span> to plan an <br> entire wedding from florals to
                        bridal party.</p>

                    <p><a href="#">See the moodboard</a></p>
                </div>
            </div>
            <div class="col-md-6 custom_padding_6">
                <div class="product_with_text_wrapper">
                    <a href="#">
                        <img src="{{ asset('themes/front/images/product/product-12.jpg') }}" alt="" class="img-fluid">
                    </a>

                    <div class="product_with_text_inner">
                        <h2>MOM: <br>BESTSELLERS <br>ARE BACK!</h2>

                        <p><a href="#">Shop Mother of the Bride</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
END PRODUCT WITH TEXT SECTION
============================== -->

<!-- =========================
START HOME PRODUCT SLIDER SECTION
============================== -->
<section class="product_slider_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Show Us How You Wore BHLDN</h3>

                <h2 class="color_pink">#BHLDNBRIDE</h2>

                <div id="product_slider2" class="common_slide owl-carousel owl-theme">
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-13.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-14.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-15.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-16.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-17.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-18.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-19.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ asset('themes/front/images/product/product-20.jpg') }}" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
END HOME PRODUCT SLIDER SECTION
============================== -->
@stop

@section('additionalJS')
<script>
$(function () {
var sliders = <?php echo json_encode($mainSliderImages)?>;

if (sliders[0].color == 'white')
    $('body').addClass('white_slide');

$('.slider').on('afterChange', function (event, slick, currentSlide) {
    if (sliders[currentSlide].color == 'white') {
        $('body').addClass('white_slide');
    } else {
        $('body').removeClass('white_slide');
    }
});
});
// Show Notification
{{--setTimeout(function () {--}}
{{--var msg = "{{str_replace("'", "\'", json_encode($welcome_msg)) }}";--}}

{{--if (msg != '')--}}
    {{--$('#modalWelcome').modal('show');--}}
{{--}, 5000);--}}
</script>
@stop