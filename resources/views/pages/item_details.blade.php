<?php
    use App\Enumeration\Availability;
    use App\Enumeration\Role;
?>

@extends('layouts.details')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('item_details', $item) }}
@stop

@section('content')
<link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <section class="single_product_area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="product_thumbnail show_desktop">

                        @foreach($item->images as $img)
                            <a class="image-link" href="{{ (!auth()->user()) ? $defaultItemImage_path : asset($img->image_path) }}">
                                <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($img->image_path) }}" class="img-fluid">
                            </a>
                        @endforeach
                    </div>
                    <div class="product_thumbnail_detail_for_mobile show_mobile">
                        <div id="product_thumbnail_detail_for_mobile" class="owl-carousel all-carousel owl-theme">
                            @foreach($item->images as $img)
                            <div class="product_thumbnail_img">
                                <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($img->image_path) }}" alt="" class="img-fluid">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="prduct_desc">
                        <div class="prduct_desc_inner">
                            <h2>{{ $item->name }}</h2>
                            <h3>
                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                    @if ($item->orig_price != null)
                                        <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                    @endif
                                    ${{ sprintf('%0.2f', $item->price) }}
                                @endif
                            </h3>
                            <p>Style No: {{ $item->style_no }}</p>
                            <?php if ( ! is_null($item->fabric) ) : ?>
                                <div>
                                    <span>Fabric: </span><label>{{ $item->fabric }}</label>
                                </div>
                            <?php endif; ?>
                            <p>
                                {!! nl2br($item->description) !!}
                            </p>
                            <div class="single_product_desc">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Color</th>
                                        <th>{{ join(' ', $sizes) }}</th>
                                        <th>Pack</th>
                                        <th>Qty</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($item->colors as $color)
                                        <tr>
                                            <td>{{ $color->name }}</td>

                                            @if ($color->pivot->available ==1)
                                                <td>
                                                    @for ($i = 1; $i <= sizeof($sizes); $i++)
                                                        <?php $p = 'pack'.$i; ?>
                                                        {{ ($item->pack->$p != null) ? $item->pack->$p : 0 }}
                                                    @endfor
                                                </td>

                                                <td>
                                                    <input type="text" class="form-control pack" data-color="{{ $color->id }}" name="input-pack[]">
                                                </td>
                                                <td class="hidden-sm-down"><span class="qty">0</span></td>

                                                <td>
                                                    @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                        <span class="price">$0.00</span>
                                                        <input type="hidden" class="input-price" value="0">
                                                    @else
                                                        <span></span>
                                                    @endif
                                                </td>
                                            @else
                                                <td class="text-center" colspan="3">Out of Stock</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <?php if ( ! is_null($item->youtube_url) ) : ?>
                                    <div class="youtube_url">
                                        <a href="#" class="btn btn-default youtube-link-btn" data-toggle="modal" data-target="#videoModal" data-theVideo="{{ $item->youtube_url }}">Youtube Video</a>
                                        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
                                            <div class="modal-dialog custom-modal-dialog modal-lg">
                                              <div class="modal-content custom-modal-content">
                                                <div class="modal-body">
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                  <div>
                                                    <iframe width="100%" height="350" src="https://www.youtube.com/embed/{{ $item->youtube_url }}"></iframe>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                
                            </div>
                            <div class="add_cart btn-primary">
                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                    <a id="btnAddToCart">Add to cart</a>
                                @else
                                    <a href="{{ route('buyer_login') }}">Login to Add to Cart</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="related_product">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="related_product_heading">
                        <h2>Similar Item</h2>
                    </div>
                    <div id="related_product" class="owl-carousel all-carousel owl-theme">
                        @foreach($suggestItems as $suggestItem)
                            <div class="related_product_inner">
                                <a href="{{ route('item_details_page', ['item' => $suggestItem->id]) }}">
                                    @if (sizeof($suggestItem->images) > 0)
                                        <img src="{{(!auth()->user()) ? $defaultItemImage_path : asset($suggestItem->images[0]->list_image_path) }}"
                                             alt="{{ (!auth()->user()) ? config('app.name') : $suggestItem->name }}" class="img-fluid">
                                    @else
                                        <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset('images/no-image.png') }}" alt="{{ (!auth()->user()) ? config('app.name') : $suggestItem->name }}" class="img-fluid">
                                    @endif
                                </a>

                                <h2>{{ $suggestItem->name }}</h2>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            var message = '{{ session('message') }}';

            if ( message != '' ) {
                toastr.success(message);
            }

            var itemInPack = {{ $itemInPack }};
            var perPrice = parseFloat('{{ $item->price }}');
            var totalQty = 0;
            var itemId = '{{ $item->id }}';

            $('.pack').keyup(function () {
                var i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        i = 0;
                }

                $(this).closest('tr').find('.qty').html(itemInPack * i);
                $(this).closest('tr').find('.price').html('$' + (itemInPack * i * perPrice).toFixed(2));
                $(this).closest('tr').find('.input-price').val(itemInPack * i * perPrice);

                calculate();

                $(this).focus();
            });

            $('#btnAddToCart').click(function () {
                var colors = [];
                var qty = [];
                var total_price = 0;
                var vendor_id = '{{ $item->vendor_meta_id }}';

                if (totalQty == 0) {
                    alert('Please select an item.');
                    return;
                }

                var valid = true;
                $('.pack').each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            return valid = false;
                    } else {
                        if (val != '')
                            return valid = false;
                    }

                    if (i != 0) {
                        colors.push($(this).data('color'));
                        qty.push(i);
                    }
                });

                if (!valid) {
                    alert('Invalid Quantity.');
                    return;
                }

                $('.input-price').each(function () {
                    total_price += parseFloat($(this).val());
                });

                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_cart') }}",
                    data: { itemId: itemId, colors: colors, qty: qty, total_price: total_price, vendor_id: vendor_id }
                }).done(function( data ) {
                    if (data.success) {
                        window.location.replace("{{ route('add_to_cart_success') }}");
                    } else {
                        alert(data.message);
                    }
                });
            });

            function calculate() {
                totalQty = 0;
                var totalPrice = 0;

                $('.qty').each(function () {
                    totalQty += parseInt($(this).html());
                });

                $('.input-price').each(function () {
                    totalPrice += parseFloat($(this).val());
                });

                $('#totalQty').html(totalQty);
                $('#totalPrice').html('$' + totalPrice.toFixed(2));
            }

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }

            // Wishlist
            $('#btnAddToWishlist').click(function () {
                var id = $(this).data('id');
                $this = $(this);

                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    toastr.success('Added to Wishlist.');
                    $this.remove();
                });
            });

            // On Back
            if (window.history && window.history.pushState) {

                $(window).on('popstate', function() {
                    localStorage['change_page'] = 1;
                    localStorage['change_pos'] = 1;
                    history.back();
                });

                window.history.pushState('forward', null, '');
            }

            
        });
    </script>
@stop