<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>

@extends('layouts.home_layout')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fotorama.css') }}">
    <style>
        .color-selected {
            border: 1px solid black !important;
        }
        .sub_category_menu
        {
            margin-left: 0px;
            padding-left: 5px;
            border-left: 1px solid #eee;
        }
    </style>
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('parent_category_page', $category) }}
@stop

@section('filters')
    <section class="product_filter_area">
        <div class="product_filter_wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="product_filter">
                            <li>VIEW</li>
                            <li class="two_grid active">2</li>
                            <li class="four_grid">4</li>
                            <li>|</li>
                            <li class="p_fiter">+Filters</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="filter_form">
            <div class="filter_form_inner">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget-categories">
                            <h6 class="widget-title">SUB CATEGORY</h6>

                            <ul>
                                @foreach($category->subCategories as $sub)
                                    <li>
                                        <a href="{{ route('second_category', ['category' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($category->name)]) }}">{{ $sub->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <h6 class="widget-title">SEARCH</h6>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-style-no" name="search-component" value="1"
                                    {{ (request()->get('search-by') == '') ? 'checked' : (request()->get('search-by') == 1 ? 'checked' : '') }}>
                            <label class="custom-control-label" for="search-style-no">Style No.</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-description" name="search-component" value="2"
                                    {{ request()->get('search-by') == 2 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="search-description">Description</label>
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="search-input" type="text" placeholder="Search"
                                   value="{{ request()->get('search') }}">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" id="search-price-min" type="text" placeholder="Price Min"
                                       value="{{ request()->get('price_min') }}">
                            </div>

                            <div class="col-md-6">
                                <input class="form-control" id="search-price-max" type="text" placeholder="Price Max"
                                       value="{{ request()->get('price_max') }}">
                            </div>
                        </div>

                        <button class="btn btn-secondary mt-3" id="btn-search">SEARCH</button>
                    </div>

                    <div class="col-md-3">
                        <h6 class="widget-title">COLORS</h6>

                        <div class="widget-categories">
                            <ul class="sidecolor">
                                @foreach($masterColors as $mc)
                                    <li class="item-color {{ request()->get('color') == $mc->id ? 'color-selected' : '' }}"
                                        style="position: relative; float: left; display: list-item; padding: 3px; border: 1px solid white"
                                        data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                        <img src="{{ asset($mc->image_path) }}" width="30px" height="20px">
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="filter_confirm text-right">
                    <a class="cancel_form" href="#">Cancel</a>
                </div>
            </div>
        </div>
    </section>
@stop

@section('content')
    <div class="container">
         <!-- =========================
            START BANNER SECTION
        ============================== -->
        <section class="banner_area common_banner clearfix">
            <div class="container container_full_width_mobile">
                <div class="row">
                    <div class="col-md-12 custom_padding_9">
                        <div class="banner_top">
                            <p>IT’S HERE: THE WEDDING SALE! | <a href="#">SHOP NEW MARKDOWNS</a> </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END BANNER SECTION
        ============================== -->
        <!-- =========================
            START BREDCRUMS SECTION
        ============================== -->
        <section class="breadcrumbs_area">
            <div class="container container_full_width_mobile">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active">@php echo $category->name; @endphp</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            START BREDCRUMS SECTION
        ============================== -->

        <!-- =========================
            START APPOINMENT SECTION
        ============================== -->
        <section class="appoinment_area common_content_area" id='main_app'>
            <div class="container">
                <div class="row">
                    <div class="col-md-2 custom_padding_9 for_desktop">
                        <div class="common_left_menu">
                            <ul>
                                <li><a href="{{url('new-arrivals')}}">View All New</a></li>
                                <ul>
                                    @foreach($default_categories as $cat)
                                    <li><a href="{{ route('category_page', ['category' => changeSpecialChar($cat['name'])]) }}">{{ $cat['name'] }}</a></li>
                                    @if(count($cat['subCategories'])>0)
                                        @foreach($cat['subCategories'] as $d_sub)
                                            <li class="sub_category_menu"><a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($cat['name'])]) }}">- {{ $d_sub['name'] }}</a></li>
                                        @endforeach
                                    @endif
                                    @endforeach
                                </ul>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div v-if='records.length > 0' class="col-md-12 custom_padding_5">
                                <div class="product_filter clearfix">
                                    <div class="product_filter_left">
                                        <select class="form-control sort_by" @change="sort_by_order()">
                                            <option value="">Sort By</option>
                                            <option value="low_to_high">Price: Low - High</option>
                                            <option value="high_to_low">Price: High - Low</option>
                                        </select>
                                    </div>
                                    <div class="product_filter_right">
                                        <ul>
                                            <li @click="pre_page_load(currenct_pagination_index)"><a href="javascript:void(0)" class="prev"><span></span></a></li>
                                            <li>@{{currenct_pagination_index}}  of @{{last_pagination_index}}</li>
                                            <li @click="next_page_load(currenct_pagination_index)"><a href="javascript:void(0)"><span></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"> 
                                <div class="row">
                                    <div v-for='record in records' class="col-6 col-md-4 col-lg-3 custom_padding_5">
                                        <div class="product_inner text-center">
                                            <a href="">
                                                <img v-if='record.images.length == 0' src="{{asset('images/no-image.png')}}" alt="" class="img-fluid">
                                                <img v-if='record.images.length != 0' :src="record.images[0]['image_path']" alt="" class="img-fluid">
                                            </a>
                                            <h2><a :href="'{{URL::to('/')}}/product/' + record.id">@{{record.name}}</a></h2>
                                            <p>$@{{record.price}}</p>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <div v-if='records.length > 0' class="col-md-12 custom_padding_5">
                                <div class="product_filter clearfix">
                                    <div class="product_filter_left">
                                        <select class="form-control sort_by" @change="sort_by_order()">
                                            <option value="">Sort By</option>
                                            <option value="low_to_high">Price: Low - High</option>
                                            <option value="high_to_low">Price: High - Low</option>
                                        </select>
                                    </div>
                                    <div class="product_filter_right">
                                        <ul>
                                            <li @click="pre_page_load(currenct_pagination_index)"><a href="#"><span></span></a></li>
                                            <li>@{{currenct_pagination_index}}  of @{{last_pagination_index}}</li>
                                            <li @click="next_page_load(currenct_pagination_index)"><a href="#"><span></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END APPOINMENT SECTION
        ============================== -->
    </div>
    


    <div class="product_grid two_grid_area two_grid_show custom-pagination">
        <div class="container-fluid">
            <div class="row" id="product-container">

            </div>
            <div class="pagination justify-content-center"></div>
        </div>
    </div>

    <template id="template-product">
        <div class="single-product col-6 col-sm-6 col-md-6 product_custom_padding">
            <div class="product_grid_inner">
                <a href="#" class="product-thumb product_grid_inner_thumb">
                    <img src="{{ asset('images/no-image.png') }}" alt="" class="lazy product-image img-fluid">
                    @if(auth()->user())
                    <span class="product_grid_inner_thumb_grid">
                        <ul>

                        </ul>
                    </span>
                    @endif
                </a>
                <h2><a class="p_title" href="#"></a></h2>
                <h3 class="price"></h3>
            </div>
        </div>
    </template>

    <template id="template-product-video">
        <div class="single-product-video col-sm-6 col-md-12">
            <div class="product_grid_inner">
                <a href="#" class="product-thumb product_grid_inner_thumb">
                    <div class="video_grid">
                        <div class="video_wrapper">
                            <video  loop muted preload="metadata" autoplay>
                                <source class="product-video" src="" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </a>
                <h2><a class="p_title" href="#"></a></h2>
                <h3 class="price"></h3>
            </div>
        </div>
    </template>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fotorama.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jqueryCookie/jquery.cookie-1.4.1.min.js') }}"></script>
    <script src="{{ asset('js/vue.js') }}"></script>
    <script src="{{ asset('js/axios.js') }}"></script>
    <script>

        var load_per_page = 5;
        var start_page_index = 1;
        var url = "{{ route('get_items_category_load_ajax') }}";
        var categories = ['{{ $category->id }}'];
        // start search for search sorting
        var app = new Vue({
            el: '#main_app',
            data: {
                records: [],
                currenct_pagination_index : 0,
                last_pagination_index : 0,
            },
            created: function() {
                this.get_all_data($('.sort_by').val(),start_page_index,load_per_page,'next');
            },
            methods: 
            {
                get_all_data: function(sort_by,offset,limit,action)
                {
                    axios.get(url,{
                        params: {
                            categories_ids: categories,
                            sort_by : sort_by,
                            offset: offset,
                            limit : limit
                        }
                    })
                    .then((response) => {
                        this.records = response.data.records;
                        this.last_pagination_index = response.data.last_pagination_index;
                        this.currenct_pagination_index = parseInt(response.data.offset);

                    })
                    .catch((err) => {
                        console.log(err);
                    });
                },
                sort_by_order: function(evt)
                {
                    this.get_all_data($('.sort_by').val(),start_page_index,load_per_page,'next');
                },
                next_page_load: function(offset_value)
                {
                    console.log(offset_value);
                    if(offset_value != this.last_pagination_index)
                    {
                        this.get_all_data($('.sort_by').val(),offset_value + 1,load_per_page,'next');
                    }
                },
                pre_page_load: function(offset_value)
                {
                    if(offset_value != 1)
                    {
                        this.get_all_data($('.sort_by').val(),offset_value - 1,load_per_page,'Pre');
                    }
                }
            }     
        });


        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            var page = 1;
            var search_text = '';
            var search_option = '';
            var search_price_min = '';
            var search_price_max = '';
            var wishlist_ids = <?php echo json_encode($wishListItems); ?>;

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);



            $('.checkbox-category, .vendor-checkbox, .checkbox-body-size, .checkbox-pattern, .checkbox-length, .checkbox-style, .checkbox-fabric').change(function () {
                filterItem();
            });

            $('#sorting').change(function () {
                filterItem();
            });

            $('.item-color').click(function () {
                if ($(this).hasClass('color-selected'))
                    $(this).removeClass('color-selected');
                else
                    $(this).addClass('color-selected');

                filterItem();
            });


            $('#btn-search').click(function () {
                search_text = $('#search-input').val();
                search_option = $('input[name=search-component]:checked').val();
                search_price_min = $('#price-range-min').val();
                search_price_max = $('#search-price-max').val();

                filterItem();
            });

            function filterItem(page) {
                page = typeof page !== 'undefined' ? page : 1;
                var categories = ['{{ $category->id }}'];
                var vendors = [];
                var masterColors = [];
                var bodySizes = [];
                var patterns = [];
                var lengths = [];
                var styles = [];
                var fabrics = [];
                var sorting = $('#sorting').val();
                var looged_in = 0;

                // Vendor
                $('.vendor-checkbox').each(function () {
                    if ($(this).is(':checked'))
                        vendors.push($(this).data('id'));
                });

                // Master Color
                $('.item-color').each(function () {
                    if ($(this).hasClass('color-selected'))
                        masterColors.push($(this).data('id'));
                });

                // Body Size
                $('.checkbox-body-size').each(function () {
                    if ($(this).is(':checked'))
                        bodySizes.push($(this).data('id'));
                });

                // Pattern
                $('.checkbox-pattern').each(function () {
                    if ($(this).is(':checked'))
                        patterns.push($(this).data('id'));
                });

                // Length
                $('.checkbox-length').each(function () {
                    if ($(this).is(':checked'))
                        lengths.push($(this).data('id'));
                });

                // Style
                $('.checkbox-style').each(function () {
                    if ($(this).is(':checked'))
                        styles.push($(this).data('id'));
                });

                // Master Fabric
                $('.checkbox-fabric').each(function () {
                    if ($(this).is(':checked'))
                        fabrics.push($(this).data('id'));
                });

                $.ajax({
                    method: "POST",
                    url: "{{ route('get_items_category') }}",
                    data: {
                        categories: categories,
                        vendors: vendors,
                        masterColors: masterColors,
                        bodySizes: bodySizes,
                        patterns: patterns,
                        lengths: lengths,
                        styles: styles,
                        fabrics: fabrics,
                        sorting: sorting,
                        searchText: search_text,
                        searchOption: search_option,
                        priceMin: search_price_min,
                        priceMax: search_price_max,
                        page: page
                    }
                }).done(function (data) {
                    var product_details_clone = $('#product_details_clone');

                    if(data.items.data == 0)
                    {
                        $('.sorting_div').hide();
                    }
                    else
                    {
                        $('.sorting_div').show();
                    }
                    
                    // console.log(data.items.data);
                    for(var i=0;i<data.items.data.length;i++)
                    {
                        product_details_clone.find('img').attr('src',data.items.data[i]['imagePath']);
                        product_details_clone.find('h2 > a').html(data.items.data[i]['name']);
                        product_details_clone.find('p').html(data.items.data[i]['price']);
                        $('#product_details_show').append(product_details_clone.clone());
                    }

                    // var products = data.items.data;
                    // $('.pagination').html(data.pagination);
                    // $('#totalItem').html(data.items.total);

                    // $('#product-container').html('');
                    // var backOrder = '{{ Availability::$ARRIVES_SOON }}';

                    // $.each(products, function (index, product) {
                    //     if (product.video == null)
                    //         var html = $('#template-product').html();
                    //     else
                    //         var html = $('#template-product-video').html();

                    //     var row = $(html);

                    //     if (product.name == null || product.name == '')
                    //         row.find('.p_title').html('&nbsp;');
                    //     else
                    //         row.find('.p_title').html(product.name);

                    //     row.find('.product-title a').attr('href', product.detailsUrl);
                    //     row.find('.product-thumb').attr('href', product.detailsUrl);
                    //     row.find('.p_title').attr('href', product.detailsUrl);
                    //     row.find('.price').html(product.price);

                    //     if (product.video == null) {
                    //         var defaultImagePath = "{{$defaultItemImage_path}}";
                    //         @if(!auth()->user())
                    //         row.find('.product-image').attr('src', defaultImagePath);
                    //         @else
                    //         row.find('.product-image').attr('src', product.imagePath);
                    //         @endif

                    //         // Colors
                    //         $.each(product.colorsImages, function (color, imagePath) {
                    //             // row.find('.product_grid_inner_thumb_grid').find('ul').append('<li><img src="' + imagePath + '" alt="" class="img-fluid"><p>' + color + '</p></li>');
                    //             row.find('.product_grid_inner_thumb_grid').find('ul').append('<li><img src="' + imagePath + '" alt="" class="img-fluid"></li>');
                    //         });

                    //     } else {
                    //         row.find('.product-video').attr('src', product.video);
                    //     }

                    //     $('#product-container').append(row);
                    //     // console.log($.cookie('cq-view'));
                    //     if ($.cookie('cq-view')!=="undefined") {
                    //         // if ($.cookie('cq-view') == 4) {
                    //         //if ($.cookie('cq-view') == 4) {
                    //             $('.four_grid').trigger('click');
                    //         //}
                    //     }
                    // });

                    // var pos = 0;
                    // var changePos = localStorage['change_pos'];
                    // if (changePos) {
                    //     localStorage.removeItem('change_pos');

                    //     pos = parseInt(localStorage.getItem('previous_position'));
                    // }

                    // $("html, body").animate({ scrollTop: pos }, "fast");
                });
            }

            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                page = getURLParameter(url, 'page');

                filterItem(page);
            });

            // Click to Show Quick View Functionalities

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
            }

            // Hold Position
            $(window).bind('beforeunload', function(){
                localStorage['previous_page'] = page;
                localStorage['previous_position'] = $(document).scrollTop()+'';
            });

            var changePage = localStorage['change_page'];
            if (changePage) {
                localStorage.removeItem('change_page');

                page = parseInt(localStorage.getItem('previous_page'));
            }


            filterItem(page);

            // 2/4 View
            $('.four_grid').click(function () {
                $.cookie('cq-view', 4);
            });

            $('.two_grid').click(function () {
                $.cookie('cq-view', 2);
            });
        });

        document.addEventListener("DOMContentLoaded", function() {
        var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));;

        if ("IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
            let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                let lazyImage = entry.target;
                lazyImage.src = lazyImage.dataset.src;
                lazyImage.srcset = lazyImage.dataset.srcset;
                lazyImage.classList.remove("lazy");
                lazyImageObserver.unobserve(lazyImage);
                }
            });
            });

            lazyImages.forEach(function(lazyImage) {
            lazyImageObserver.observe(lazyImage);
            });
        }
        });
    </script>
@stop