<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>

@extends('layouts.home_layout')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fotorama.css') }}">
    <style>
        .color-selected {
            border: 1px solid black !important;
        }
    </style>
@stop

@section('breadcrumbs')
    {{ Breadcrumbs::render('second_parent_category_page', $category) }}
@stop


@section('filters')
    <section class="product_filter_area">
        <div class="product_filter_wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="product_filter">
                            <li>VIEW</li>
                            <li class="two_grid active">2</li>
                            <li class="four_grid">4</li>
                            <li>|</li>
                            <li class="p_fiter">+Filters</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="filter_form">
            <div class="filter_form_inner">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget-categories">
                            <h6 class="widget-title">SUB CATEGORY</h6>

                            <ul>
                                @foreach($category->parentCategory->subCategories as $sub)
                                    @if ($sub->id == $category->id)
                                        <li class="has-children expanded"><a href="#">{{ $sub->name }}</a></span>
                                            <ul>
                                                @foreach($category->subCategories as $sub2)
                                                    <li>
                                                        <div class="custom-control custom-checkbox">
                                                            <input class="custom-control-input checkbox-category" type="checkbox"
                                                                   data-id="{{ $sub2->id }}" id="category_mobile_{{ $sub2->id }}">
                                                            <label class="custom-control-label" for="category_mobile_{{ $sub2->id }}">{{ $sub2->name }} ({{ $sub2->totalItem }})</label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li><a href="{{ route('second_category', ['category' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($sub->parentCategory->name)]) }}">{{ $sub->name }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <h6 class="widget-title">SEARCH</h6>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-style-no" name="search-component" value="1"
                                    {{ (request()->get('search-by') == '') ? 'checked' : (request()->get('search-by') == 1 ? 'checked' : '') }}>
                            <label class="custom-control-label" for="search-style-no">Style No.</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="search-description" name="search-component" value="2"
                                    {{ request()->get('search-by') == 2 ? 'checked' : '' }}>
                            <label class="custom-control-label" for="search-description">Description</label>
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="search-input" type="text" placeholder="Search"
                                   value="{{ request()->get('search') }}">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" id="search-price-min" type="text" placeholder="Price Min"
                                       value="{{ request()->get('price_min') }}">
                            </div>

                            <div class="col-md-6">
                                <input class="form-control" id="search-price-max" type="text" placeholder="Price Max"
                                       value="{{ request()->get('price_max') }}">
                            </div>
                        </div>

                        <button class="btn btn-secondary mt-3" id="btn-search">SEARCH</button>
                    </div>

                    <div class="col-md-3">
                        <h6 class="widget-title">COLORS</h6>

                        <div class="widget-categories">
                            <ul class="sidecolor">
                                @foreach($masterColors as $mc)
                                    <li class="item-color {{ request()->get('color') == $mc->id ? 'color-selected' : '' }}"
                                        style="position: relative; float: left; display: list-item; padding: 3px; border: 1px solid white"
                                        data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                        <img src="{{ asset($mc->image_path) }}" width="30px" height="20px">
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="filter_confirm text-right">
                    <a class="cancel_form" href="#">Cancel</a>
                </div>
            </div>
        </div>
    </section>
@stop

@section('content')
    <div class="product_grid two_grid_area two_grid_show custom-pagination">
        <div class="container-fluid">
            <div class="row" id="product-container">

            </div>
            <div class="pagination justify-content-center"></div>
        </div>
    </div>

    <template id="template-product">
        <div class="single-product col-6 col-sm-6 col-md-6 product_custom_padding">
            <div class="product_grid_inner">
                <a href="#" class="product-thumb product_grid_inner_thumb">
                    <img src="{{ asset('images/no-image.png') }}" alt="" class="lazy product-image img-fluid">
                    @if(auth()->user())
                    <span class="product_grid_inner_thumb_grid">
                        <ul>

                        </ul>
                    </span>
                    @endif
                </a>
                <h2><a class="p_title" href="#"></a></h2>
                <h3 class="price"></h3>
            </div>
        </div>
    </template>

    <template id="template-product-video">
        <div class="single-product-video col-sm-6 col-md-12">
            <div class="product_grid_inner">
                <a href="#" class="product-thumb product_grid_inner_thumb">
                    <div class="video_grid">
                        <div class="video_wrapper">
                            <video  loop muted preload="metadata" autoplay>
                                <source class="product-video" src="" type="video/mp4">
                            </video>
                        </div>
                    </div>
                </a>
                <h2><a class="p_title" href="#"></a></h2>
                <h3 class="price"></h3>
            </div>
        </div>
    </template>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fotorama.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jqueryCookie/jquery.cookie-1.4.1.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var page = 1;
            var search_text = '';
            var search_option = '';
            var search_price_min = '';
            var search_price_max = '';
            var wishlist_ids = <?php echo json_encode($wishListItems); ?>;

            
            $('.checkbox-category, .vendor-checkbox, .checkbox-body-size, .checkbox-pattern, .checkbox-length, .checkbox-style, .checkbox-fabric').change(function () {
                filterItem();
            });

            $('#sorting').change(function () {
                filterItem();
            });

            $('.item-color').click(function () {
                if ($(this).hasClass('color-selected'))
                    $(this).removeClass('color-selected');
                else
                    $(this).addClass('color-selected');

                filterItem();
            });
            
            $('#btn-search').click(function () {
                search_text = $('#search-input').val();
                search_option = $('input[name=search-component]:checked').val();
                search_price_min = $('#search-price-min').val();
                search_price_max = $('#search-price-max').val();

                filterItem();
            });

            function filterItem(page) {
                page = typeof page !== 'undefined' ? page : 1;
                var secondCategory = '{{ $category->id }}';
                var categories = [];
                var vendors = [];
                var masterColors = [];
                var bodySizes = [];
                var patterns = [];
                var lengths = [];
                var styles = [];
                var fabrics = [];
                var sorting = $('#sorting').val();

                // Category
                $('.checkbox-category').each(function () {
                    if ($(this).is(':checked'))
                        categories.push($(this).data('id'));
                });

                // Vendor
                $('.vendor-checkbox').each(function () {
                    if ($(this).is(':checked'))
                        vendors.push($(this).data('id'));
                });
                
                // Master Color
                $('.item-color').each(function () {
                    if ($(this).hasClass('color-selected'))
                        masterColors.push($(this).data('id'));
                });

                // Body Size
                $('.checkbox-body-size').each(function () {
                    if ($(this).is(':checked'))
                        bodySizes.push($(this).data('id'));
                });

                // Pattern
                $('.checkbox-pattern').each(function () {
                    if ($(this).is(':checked'))
                        patterns.push($(this).data('id'));
                });

                // Length
                $('.checkbox-length').each(function () {
                    if ($(this).is(':checked'))
                        lengths.push($(this).data('id'));
                });

                // Style
                $('.checkbox-style').each(function () {
                    if ($(this).is(':checked'))
                        styles.push($(this).data('id'));
                });

                // Master Fabric
                $('.checkbox-fabric').each(function () {
                    if ($(this).is(':checked'))
                        fabrics.push($(this).data('id'));
                });


                $.ajax({
                    method: "POST",
                    url: "{{ route('get_items_sub_category') }}",
                    data: { categories: categories, secondCategory: secondCategory, vendors: vendors, masterColors: masterColors, bodySizes: bodySizes, patterns: patterns,
                        lengths: lengths, styles: styles, fabrics: fabrics, sorting: sorting, searchText: search_text, searchOption: search_option,
                        priceMin: search_price_min, priceMax: search_price_max, page: page
                    }
                }).done(function( data ) {
                    var products = data.items.data;
                    $('.pagination').html(data.pagination);
                    $('#totalItem').html(data.items.total);

                    $('#product-container').html('');
                    var backOrder = '{{ Availability::$ARRIVES_SOON }}';

                    $.each(products, function (index, product) {
                        if (product.video == null)
                            var html = $('#template-product').html();
                        else
                            var html = $('#template-product-video').html();

                        var row = $(html);

                        if (product.name == null || product.name == '')
                            row.find('.p_title').html('&nbsp;');
                        else
                            row.find('.p_title').html(product.name);

                        row.find('.product-title a').attr('href', product.detailsUrl);
                        row.find('.product-thumb').attr('href', product.detailsUrl);
                        row.find('.p_title').attr('href', product.detailsUrl);
                        row.find('.price').html(product.price);


                        if (product.video == null) {
                            var defaultImagePath = "{{$defaultItemImage_path}}";
                            @if(!auth()->user())
                            row.find('.product-image').attr('src', defaultImagePath);
                            @else
                            row.find('.product-image').attr('src', product.imagePath);
                            @endif
                            // Colors
                            $.each(product.colorsImages, function (color, imagePath) {
                                // row.find('.product_grid_inner_thumb_grid').find('ul').append('<li><img src="' + imagePath + '" alt="" class="img-fluid"><p>' + color + '</p></li>');
                                row.find('.product_grid_inner_thumb_grid').find('ul').append('<li><img src="' + imagePath + '" alt="" class="img-fluid"></li>');
                            });
                        } else {
                            row.find('.product-video').attr('src', product.video);
                        }

                        $('#product-container').append(row);

                        if ($.cookie('cq-view')!=="undefined") {
                            // if ($.cookie('cq-view') == 4) {
                                $('.four_grid').trigger('click');
                            // }
                        }
                    });

                    var pos = 0;
                    var changePos = localStorage['change_pos'];
                    if (changePos) {
                        localStorage.removeItem('change_pos');

                        pos = parseInt(localStorage.getItem('previous_position'));
                    }

                    $("html, body").animate({ scrollTop: pos }, "fast");
                });
            }

            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                page = getURLParameter(url, 'page');

                filterItem(page);
            });

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
            }

            // Hold Position
            $(window).bind('beforeunload', function(){
                localStorage['previous_page'] = page;
                localStorage['previous_position'] = $(document).scrollTop()+'';
            });

            var changePage = localStorage['change_page'];
            if (changePage) {
                localStorage.removeItem('change_page');

                page = parseInt(localStorage.getItem('previous_page'));
            }

            filterItem(page);

            // 2/4 View
            $('.four_grid').click(function () {
                $.cookie('cq-view', 4);
            });

            $('.two_grid').click(function () {
                $.cookie('cq-view', 2);
            });
        });

        document.addEventListener("DOMContentLoaded", function() {
        var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));;

        if ("IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
            let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                let lazyImage = entry.target;
                lazyImage.src = lazyImage.dataset.src;
                lazyImage.srcset = lazyImage.dataset.srcset;
                lazyImage.classList.remove("lazy");
                lazyImageObserver.unobserve(lazyImage);
                }
            });
            });

            lazyImages.forEach(function(lazyImage) {
            lazyImageObserver.observe(lazyImage);
            });
        }
        });
    </script>
@stop